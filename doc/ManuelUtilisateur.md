# Manuel Utilisateur

## Index
- [Manuel Utilisateur](#manuel-utilisateur)
  - [Index](#index)
  - [Introduction](#introduction)
  - [Débuts](#d%c3%a9buts)
  - [Menu principal](#menu-principal)
  - [Menu simplifié de commande](#menu-simplifi%c3%a9-de-commande)
    - [**1. cd**](#1-cd)
      - [Fonction](#fonction)
      - [Exemple](#exemple)
    - [**2. pwd**](#2-pwd)
      - [Fonction](#fonction-1)
      - [Exemple](#exemple-1)
    - [**3. ls**](#3-ls)
      - [Fonction](#fonction-2)
      - [Exemple sans l'option](#exemple-sans-loption)
      - [Exemple avec l'option](#exemple-avec-loption)
    - [**4. touch**](#4-touch)
      - [Fonction](#fonction-3)
      - [Exemple répertoire courant](#exemple-r%c3%a9pertoire-courant)
      - [Exemple répertoire non courant](#exemple-r%c3%a9pertoire-non-courant)
    - [**5. mkdir**](#5-mkdir)
      - [Fonction](#fonction-4)
      - [Exemple répertoire courant](#exemple-r%c3%a9pertoire-courant-1)
      - [Exemple répertoire non courant](#exemple-r%c3%a9pertoire-non-courant-1)
    - [**6. rm**](#6-rm)
      - [Fonction](#fonction-5)
      - [Exemple répertoire courant](#exemple-r%c3%a9pertoire-courant-2)
      - [Exemple répertoire non courant](#exemple-r%c3%a9pertoire-non-courant-2)
      - [Exemple avec option](#exemple-avec-option)
    - [**7. vim**](#7-vim)
      - [Fonction](#fonction-6)
      - [Exemple](#exemple-2)
  - [Terminal](#terminal)
    - [**help**](#help)
      - [Fonction](#fonction-7)
      - [Synopsis](#synopsis)
      - [Exemple](#exemple-3)
    - [**cd**](#cd)
      - [Fonction](#fonction-8)
      - [Synopsis](#synopsis-1)
      - [Exemples](#exemples)
    - [**pwd**](#pwd)
      - [Fonction](#fonction-9)
      - [Synopsis](#synopsis-2)
      - [Exemples](#exemples-1)
    - [**ls**](#ls)
      - [Fonction](#fonction-10)
      - [Synopsis](#synopsis-3)
      - [Exemples](#exemples-2)
    - [**touch**](#touch)
      - [Fonction](#fonction-11)
      - [Synopsis](#synopsis-4)
      - [Exemples](#exemples-3)
    - [**mkdir**](#mkdir)
      - [Fonction](#fonction-12)
      - [Synopsis](#synopsis-5)
      - [Exemples](#exemples-4)
    - [**rm**](#rm)
      - [Fonction](#fonction-13)
      - [Synopsis](#synopsis-6)
      - [Exemples](#exemples-5)
    - [**vim**](#vim)
      - [Fonction](#fonction-14)
      - [Synopsis](#synopsis-7)
      - [Exemple](#exemple-4)
<br><br>

## Introduction
***

Un **Système de Gestion de Fichiers** est une organisation hiérarchique de fichiers et de dossiers dans laquelle il est possible de se déplacer, de lister, de créer ou encore de supprimer des fichiers ou dossiers.\
<br>

## Débuts
***

Le programme se lance via le fichier <span style="color:#2ECC71">**main_sgf**</span> situé dans le dossier compressé suivant :

<pre>
<span style="color:#2C3E50">\-<span style="color:#2980B9"><b>SGF</span>
   \-<span style="color:#2980B9">doc</span>
   \-<span style="color:#2980B9">src</span>
   \-<span style="color:#2980B9">livrables</span>
      |-<span style="color:#2ECC71">main_sgf</span>
      |-<span style="color:#2ECC71">p_tree.o</span>
      |-<span style="color:#2ECC71">p_dms.o</span>
      |-<span style="color:#2ECC71">p_commandfuncs.o</b></span></span>
</pre>

Les fichiers <span style="color:#2ECC71">**p_tree.o**</span>, <span style="color:#2ECC71">**p_dms.o**</span> et <span style="color:#2ECC71">**p_commandfuncs.o**</span> sont des packages permet d'implémenter toutes les fonctionnalitées du **SGF**

 Le programme se lance avec la commande suivante :

<pre style="color:#2C3E50">
<b>./main_sgf</b>
</pre>
<br>

## Menu principal
***

Une fois que le programme sera lancé, vous arriverez sur la fenêtre suivante : 
<pre>
<span style="color:#2980B9"><b>############################
<span>#      Welcome to SGF      #</span>
############################</span>
<span style="color:#2ECC71">You can choose between :
(<span style="color:#E67E22">P</span>)rompt command
(<span style="color:#E67E22">M</span>)enu
Choice :</span></b>
</pre>
\
Si vous choisissez <span style="color:#E67E22">**P**</span>, vous aurez accès à un terminal comme vous pouvez retrouver sous linux.
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">P</span></span></b>
</pre>
\
Si vous choisissez <span style="color:#E67E22">**M**</span>, vous aurez accès à un menu simplifié permettant de réaliser les commandes sans les connaître tout en étant guidé.
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">M</span></span></b>
</pre>
<br>

## Menu simplifié de commande
***

Dans ce menu, vous trouverez une liste de commandes dans laquelle il sera possible d'appliquer les mêmes commandes que dans un terminal sans pour autant les connaître.

Le menu est le suivant :
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : </b></span>
</pre>
\
Vous avez donc le choix entre ces <span style="color:#E67E22">**6**</span> commandes.\
<br>

### **1. cd**
***

#### Fonction
La commande <span style="color:#E67E22">**cd**</span> permet de changer de répertoire courant en spécifiant le répertoire de déstination.
<br><br>

#### Exemple
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">1</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Change directory : <span style="color:#E67E22">/</span> 
Current Directory : <span style="color:#9B59B6">/</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">1</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Puis vous choisissez le répertoire dans lequel vous voulez vous déplacer
<pre>
<span style="color:#2ECC71"><b>Change directory : <span style="color:#E67E22">/</span></b>
</pre>
<br>
Et enfin le menu vous indique le nouveau répertoire dans lequel vous vous trouvez désormais.
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/</span></b>
</pre>
<br>

### **2. pwd**
***

#### Fonction
La commande <span style="color:#E67E22">**pwd**</span> permet d'afficher le répertoire courant.
<br><br>

#### Exemple
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">2</span>
Current Directory : <span style="color:#9B59B6">/hello/</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">2</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>

### **3. ls**
***

#### Fonction
La commande <span style="color:#E67E22">**ls**</span> permet de lister les dossiers ou fichiers enfants du répertoire courant. De plus il y a possibilité d'ajouter une option afin de lister plus en détail les fichiers et dossier sous la forme :
<pre>
<b><span style="color:#2ECC71">droits taille extension nom</span></b>
</pre>
- <span style="color:#2ECC71">**droits**</span> : Défini sous la forme <span style="color:#9B59B6">***?rwx***</span> avec <span style="color:#9B59B6">***?***</span> qui peut prendre comme valeur <span style="color:#E67E22">***d***</span> qui signifie que c'est un dossier ou <span style="color:#E67E22">***-***</span> qui signifie que c'est un fichier. <span style="color:#9B59B6">***rwx***</span> qui signifie respectivement les droits de lecture, d'écriture et d'éxécution.
- <span style="color:#2ECC71">**taille**</span> : Défini la taille du dossier ou fichier en octets.
- <span style="color:#2ECC71">**extension**</span> : Défini l'extension du fichier et n'affiche rien si c'est un dossier.
- <span style="color:#2ECC71">**nom**</span> : Défini le nom du fichier ou dossier.
<br><br>

#### Exemple sans l'option

<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">3</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Option :
<span style="color:#9B59B6">sup test.txt www</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">3</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir de ne pas mettre d'option 
<pre>
<span style="color:#2ECC71"><b>Option : </b>
</pre>
<br>
Enfin, le menu vous affiche les différents dossiers et fichiers si ils existent
<pre>
<b><span style="color:#9B59B6">sup test.txt www</span></b></span>
</pre>
<br>

#### Exemple avec l'option
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">3</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Option : <span style="color:#E67E22">-l</span>
<span style="color:#9B59B6">drwx           0  sup
-rwx           0 .txt test.txt
drwx           0  www</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">3</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir l'option <b>-l</b> pour pouvoir lister en détail les éléments
<pre>
<span style="color:#2ECC71"><b>Option : <span style="color:#E67E22">-l</span></b>
</pre>
<br>
Enfin, le menu vous affiche les différents dossiers et fichiers si ils existent
<pre>
<b><span style="color:#9B59B6">drwx           0  sup
-rwx           0 .txt test.txt
drwx           0  www</span></b></span>
</pre>
<br>

### **4. touch**
***

#### Fonction
La commande <span style="color:#E67E22">**touch**</span> permet de créer un fichier dans le répertoire courant ou dans un autre répertoire existant.
<br><br>

#### Exemple répertoire courant
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">4</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Change directory and add folder name : <span style="color:#E67E22">adaIsAGoodLanguage.adb</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">4</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir le nom du fichier
<pre>
<span style="color:#2ECC71"><b>Change directory and add folder name : <span style="color:#E67E22">adaIsAGoodLanguage.adb</span></b></span></span></b>
</pre>
<br>
Vous avez donc créé un fichier <b><span style="color:#E67E22">adaIsAGoodLanguage.adb</span></b> dans le répertoire <b><span style="color:#2ECC71">/hello/</span></b>
<br><br>

#### Exemple répertoire non courant
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">4</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Change directory and add folder name : <span style="color:#E67E22">/adaIsAGoodLanguage.adb</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">4</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir le nom du fichier
<pre>
<span style="color:#2ECC71"><b>Change directory and add folder name : <span style="color:#E67E22">/adaIsAGoodLanguage.adb</span></b></span></span></b>
</pre>
<br>
Vous avez donc créé un fichier <b><span style="color:#E67E22">adaIsAGoodLanguage.adb</span></b> dans le répertoire <b><span style="color:#2ECC71">/</span></b>
<br><br>

### **5. mkdir**
***

#### Fonction
La commande <span style="color:#E67E22">**mkdir**</span> permet de créer un dossier dans le répertoire courant ou dans un autre répertoire existant.
<br><br>

#### Exemple répertoire courant
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">5</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Change directory and add folder name : <span style="color:#E67E22">Project</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">5</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir le nom du dossier
<pre>
<span style="color:#2ECC71"><b>Change directory and add folder name : <span style="color:#E67E22">Project</span></b></span></span></b>
</pre>
<br>
Vous avez donc créé un dossier <b><span style="color:#E67E22">Project</span></b> dans le répertoire <b><span style="color:#2ECC71">/hello/</span></b>
<br><br>

#### Exemple répertoire non courant
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">5</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Change directory and add folder name : <span style="color:#E67E22">/Project</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">5</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir le nom du dossier
<pre>
<span style="color:#2ECC71"><b>Change directory and add folder name : <span style="color:#E67E22">/Project</span></b></span></span></b>
</pre>
<br>
Vous avez donc créé un fichier <b><span style="color:#E67E22">Project</span></b> dans le répertoire <b><span style="color:#2ECC71">/</span></b>
<br><br>

### **6. rm**
***

#### Fonction
La commande <span style="color:#E67E22">**rm**</span> permet de supprimer un fichier ou un dossier du répertoire courant ou d'un autre répertoire.
<br><br>

#### Exemple répertoire courant
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">6</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Option : <span style="color:#E67E22"></span>
Change directory and add folder or file to remove : <span style="color:#E67E22">test.txt</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">6</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir de ne pas mettre d'option 
<pre>
<span style="color:#2ECC71"><b>Option : </b>
</pre>
<br>
Vous pouvez ensuite choisir le nom du fichier à supprimer
<pre>
<span style="color:#2ECC71"><b>Change directory and add folder or file to remove : <span style="color:#E67E22">test.txt</span></b></span></span></b>
</pre>
<br>
Vous avez donc supprimer le fichier <b><span style="color:#E67E22">test.txt</span></b> dans le répertoire <b><span style="color:#2ECC71">/hello/</span></b>
<br><br>

#### Exemple répertoire non courant
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">6</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Option : <span style="color:#E67E22"></span>
Change directory and add folder name : <span style="color:#E67E22">/test.txt</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">6</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir de ne pas mettre d'option 
<pre>
<span style="color:#2ECC71"><b>Option : </b>
</pre>
<br>
Vous pouvez ensuite choisir le nom du fichier à supprimer
<pre>
<span style="color:#2ECC71"><b>Change directory and add folder name : <span style="color:#E67E22">/test.txt</span></b></span></span></b>
</pre>
<br>
Vous avez donc supprimer le fichier <b><span style="color:#E67E22">test.txt</span></b> dans le répertoire <b><span style="color:#2ECC71">/</span></b>
<br><br>

#### Exemple avec option
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">6</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Option : <span style="color:#E67E22">-r</span>
Change directory and add folder name : <span style="color:#E67E22">Project</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">6</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir de mettre l'option <b><span style="color:#E67E22">-r</span></b> qui permet de supprimer de manière récursive un dossier  
<pre>
<span style="color:#2ECC71"><b>Option : <span style="color:#E67E22">-r</span></b>
</pre>
<br>
Vous pouvez ensuite choisir le nom du dossier à supprimer
<pre>
<span style="color:#2ECC71"><b>Change directory and add folder name : <span style="color:#E67E22">Project</span></b></span></span></b>
</pre>
<br>
Vous avez donc supprimer le dossier <b><span style="color:#E67E22">Project</span></b> dans le répertoire <b><span style="color:#2ECC71">/hello</span></b>
<br><br>

### **7. vim**
***

#### Fonction
La commande <span style="color:#E67E22">**vim**</span> permet de changer la taille d'un fichier.
<br><br>

#### Exemple
<pre>
<span style="color:#2980B9"><b>############################
<span>#       Command Menu       #</span>
############################</span>
<span style="color:#2ECC71">You can choose between : (Please enter the number of the command)
<span style="color:#E67E22">1</span>. cd : change directory
<span style="color:#E67E22">2</span>. pwd : print working directory
<span style="color:#E67E22">3</span>. ls : list files and directories
<span style="color:#E67E22">4</span>. touch : create file
<span style="color:#E67E22">5</span>. mkdir : create directory
<span style="color:#E67E22">6</span>. rm : remove folder or file
<span style="color:#E67E22">7</span>. vim : change size of the file
Choice : <span style="color:#E67E22">7</span>
Current Directory : <span style="color:#9B59B6">/hello/</span>
Change directory and file to change : <span style="color:#E67E22">test.txt</span>
New size of file : <span style="color:#E67E22">30</span></b></span>
</pre>
<br>
Après avoir fait votre choix
<pre>
<span style="color:#2ECC71"><b>Choice : <span style="color:#E67E22">7</span></b>
</pre>
<br>
Le menu vous montre dans quel répertoire vous vous trouvez
<pre>
<span style="color:#2ECC71"><b>Current Directory : <span style="color:#9B59B6">/hello/</span></b>
</pre>
<br>
Vous pouvez ensuite choisir le fichier que vous voulez dans le répertoire que vous voulez  
<pre>
<span style="color:#2ECC71"><b>Change directory and file to change : <span style="color:#E67E22">test.txt</span></b>
</pre>
<br>
Vous pouvez ensuite choisir la nouvelle taille du fichier
<pre>
<span style="color:#2ECC71"><b>New size of file : <span style="color:#E67E22">30</span></b></span></span></b>
</pre>
<br>
Vous avez donc changer la taille du fichier <b><span style="color:#E67E22">test.txt</span></b>
<br><br>

## Terminal
***
Dans le terminal vous trouverez les mêmes fonctionnalitées que dans le menu simplifié sous forme d'un interpréteur de commande représenté comme ceci :
<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ </span></b>
</pre>
<br>
Vous pouvez donc entrez des commandes comme dans n'importe quel interpréteur de commande. Si vous entrez une commande inconnue le terminal ne fera rien et continuera son éxécution classique.

Vous trouverez donc dans le terminal toutes les commandes suivantes : <span style="color:#2ECC71">**help**</span>, <span style="color:#2ECC71">**cd**</span>, <span style="color:#2ECC71">**pwd**</span>, <span style="color:#2ECC71">**ls**</span>, <span style="color:#2ECC71">**touch**</span>, <span style="color:#2ECC71">**mkdir**</span> et <span style="color:#2ECC71">**rm**</span>. Toutes ces commandes seront expliqué ci-dessous.
<br><br>

### **help**
***

#### Fonction

La commande <span style="color:#E67E22"><b>help</b></span> permet de rappeler toutes les commandes disponibles ainsi qu'un bref rappelle de leur fonction.
<br><br>

#### Synopsis
<pre>
<span style="color:#2C3E50"><b>help</b></span>
</pre>
<br>

#### Exemple

<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ help</span>
<span style="color:#9B59B6">cd</span>      <span style="color:#2980B9">Change directory
<span style="color:#9B59B6">help</span>    <span style="color:#2980B9">Display commands
<span style="color:#9B59B6">ls</span>      <span style="color:#2980B9">Display folder(s) and file(s)
<span style="color:#9B59B6">mkdir</span>   <span style="color:#2980B9">Create a folder
<span style="color:#9B59B6">pwd</span>     <span style="color:#2980B9">Display the current directory
<span style="color:#9B59B6">rm</span>      <span style="color:#2980B9">Remove folder or file
<span style="color:#9B59B6">touch</span>   <span style="color:#2980B9">Create a file</b>
</pre>
<br>

### **cd**
***

#### Fonction
La commande <span style="color:#E67E22">**cd**</span> permet de changer de répertoire courant en spécifiant le répertoire de déstination.
<br><br>

#### Synopsis
<pre>
<b><span style="color:#2C3E50">cd <span style="color:#9B59B6">[<i>directory</i>]</span></span></b>
</pre>
<br>

#### Exemples

<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ cd <span style="color:#9B59B6">/</span></span>
<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/</span><span style="color:#2C3E50">$ cd <span style="color:#9B59B6">/hello/test</span></span>
<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/test/</span><span style="color:#2C3E50">$ cd <span style="color:#9B59B6">..</span></span>
<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$</span></b>
</pre>
<br>

### **pwd**
***

#### Fonction
La commande <span style="color:#E67E22">**pwd**</span> permet d'afficher le répertoire courant.
<br><br>

#### Synopsis
<pre>
<b><span style="color:#2C3E50">pwd</span></b>
</pre>
<br>

#### Exemples

<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ pwd</span>
<span style="color:#E67E22">/hello/</span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/test/</span><span style="color:#2C3E50">$ pwd</span>
<span style="color:#E67E22">/hello/test/</span></span></b>
</pre>
<br>

### **ls**
***

#### Fonction
La commande <span style="color:#E67E22">**ls**</span> permet de lister les dossiers ou fichiers enfants du répertoire courant. De plus il y a possibilité d'ajouter une option afin de lister plus en détail les fichiers et dossier sous la forme :

<pre>
<b><span style="color:#2ECC71">droits taille extension nom</span></b>
</pre>
- <span style="color:#2ECC71">**droits**</span> : Défini sous la forme <span style="color:#9B59B6">***?rwx***</span> avec <span style="color:#9B59B6">***?***</span> qui peut prendre comme valeur <span style="color:#E67E22">***d***</span> qui signifie que c'est un dossier ou <span style="color:#E67E22">***-***</span> qui signifie que c'est un fichier. <span style="color:#9B59B6">***rwx***</span> qui signifie respectivement les droits de lecture, d'écriture et d'éxécution.
- <span style="color:#2ECC71">**taille**</span> : Défini la taille du dossier ou fichier en octets.
- <span style="color:#2ECC71">**extension**</span> : Défini l'extension du fichier et n'affiche rien si c'est un dossier.
- <span style="color:#2ECC71">**nom**</span> : Défini le nom du fichier ou dossier.
<br><br>

#### Synopsis
<pre>
<b><span style="color:#2C3E50">ls</span> <span style="color:#34495E">[-l]</span> <span style="color:#9B59B6">[<i>directory</i>]</span></b>
</pre>
<br>

#### Exemples
<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ ls</span>
<span style="color:#E67E22">sup test.txt www</span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ ls <span style="color:#34495E">-l</span></span>
<span style="color:#E67E22">drwx           0  sup
-rwx           0 .txt test.txt
drwx           0  www</span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ ls <span style="color:#9B59B6">sup</span></span>
<span style="color:#E67E22">adaIsAGoodLanguage.adb random</span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ ls <span style="color:#34495E">-l</span> <span style="color:#9B59B6">sup</span></span>
<span style="color:#E67E22">-rwx           0 .adb adaIsAGoodLanguage.adb
drwx           0  random</span></b>
</pre>
<br>

### **touch**
***

#### Fonction
La commande <span style="color:#E67E22">**touch**</span> permet de créer un fichier dans le répertoire courant ou dans un autre répertoire existant.
<br><br>

#### Synopsis
<pre>
<b><span style="color:#2C3E50">touch</span> <span style="color:#9B59B6">[<i>file</i>]</span></b>
</pre>
<br>

#### Exemples
<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ touch <span style="color:#9B59B6">adaIsAGoodLanguage.adb</span></span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ touch <span style="color:#9B59B6">../SGF.adb</span></span></b>
</pre>
<br>

### **mkdir**
***

#### Fonction
La commande <span style="color:#E67E22">**mkdir**</span> permet de créer un dossier dans le répertoire courant ou dans un autre répertoire existant.
<br><br>

#### Synopsis
<pre>
<b><span style="color:#2C3E50">mkdir</span> <span style="color:#9B59B6"><i>directory</i></span></b>
</pre>
<br>

#### Exemples
<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ mkdir <span style="color:#9B59B6">Project</span></span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ mdkir <span style="color:#9B59B6">../SGF</span></span></b>
</pre>
<br>

### **rm**
***

#### Fonction
La commande <span style="color:#E67E22">**rm**</span> permet de supprimer un fichier ou un dossier du répertoire courant ou d'un autre répertoire.
<br><br>

#### Synopsis
<pre>
<b><span style="color:#2C3E50">rm</span> <span style="color:#34495E">[-r [<i>directory</i>]]</span> <span style="color:#9B59B6">[<i>file</i>]</span></b>
</pre>
<br>

#### Exemples
<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ rm <span style="color:#9B59B6">test.txt</span></span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ rm <span style="color:#9B59B6">../SGF.adb</span></span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ rm -r <span style="color:#9B59B6">Project</span></span>

<span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ rm -r <span style="color:#9B59B6">../SGF</span></span></b>
</pre>
<br>

### **vim**
***

#### Fonction
La commande <span style="color:#E67E22">**vim**</span> permet de changer la taille d'un fichier.
<br><br>

#### Synopsis
<pre>
<b><span style="color:#2C3E50">vim</span> <span style="color:#9B59B6">[<i>file</i>]</span> <span style="color:#9B59B6">[<i>size</i>]</span></b>
</pre>
<br>

#### Exemple
<pre>
<b><span style="color:#2ECC71">QRostalski@ComputerN7</span><span style="color:#2C3E50">:</span><span style="color:#2980B9">/hello/</span><span style="color:#2C3E50">$ vim <span style="color:#9B59B6">test.txt 30</span></span></b>
</pre>