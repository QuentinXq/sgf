# Projet SGF

- [Projet SGF](#projet-sgf)
  - [Objectif](#objectif)
  - [Introduction](#introduction)
  - [Organisation de mon travail](#organisation-de-mon-travail)
  - [Choix](#choix)
  - [Arborescence](#arborescence)
  - [Types de données](#types-de-donn%c3%a9es)
    - [Arbre](#arbre)
    - [Chemin (path)](#chemin-path)
    - [Commande](#commande)
  - [Architecture de l'application en modules](#architecture-de-lapplication-en-modules)
    - [main_sgf](#mainsgf)
    - [p_tree](#ptree)
    - [p_commandfuncs](#pcommandfuncs)
    - [p_dms](#pdms)
  - [Tests](#tests)
  - [Difficultés et Solutions](#difficult%c3%a9s-et-solutions)
  - [Bilan technique](#bilan-technique)
  - [Bilan personnel](#bilan-personnel)
<br><br>


## Objectif
***

L'objectif de ce projet est de réaliser un **SGF** (Système de gestion de fichiers) qui consiste à organiser de façon hiérarchique des fichiers et des répertoires afin de pouvoir gérer, modifier, créer, lister ou encore connaître des informations sur ces derniers. Le projet sera mis en place individuellement et en langage **Ada**. Le **SGF** sera sous forme de ligne de commandes, ou sous forme de menus pour une utilisation plus simple.
<br><br>


## Introduction
***

Ce document montrera ma vision sur le problème à traiter à travers ma démarche. J'exposerai mes choix, mon avancement, mon resenti sur ce projet ainsi que les difficultés que j'ai pu rencontrer.
<br><br>

## Organisation de mon travail
***

Ayant eu très peu de projets de cette envergure en programmation dans ma scolarité, la conception a été un peu difficile au début, c'est pour cela que j'ai voulu travailler avec des personnes qui avaient déjà une expérience dans ce domaine. De ce fait, j'ai réalisé une partie de ma conception en binôme afin de comprendre les enjeux de ce projet mais surtout de ne pas me tromper sur mes choix et d'avoir un avis extérieur.
<br><br>

## Choix
***

J'ai décidé lors de ce projet de ne pas inclure la généricité ainsi que la privacité, car ce sont deux concepts que je ne maitrisais pas complètement mais sur lesquels j'ai pu réfléchir. J'ai également décidé de réaliser le moins d'exceptions possibles grâce à la programmation défensive en me protégeant de ce que les utilisateurs pourraient faire de mon programme.

## Arborescence
***

```
\-SGF
   \-doc
      |-SGF_QR.pdf
      |-ManuelUtilisateur.pdf
   \-src
      |-main_sgf.adb
      |-p_tree.ads
      |-p_tree.adb
      |-p_dms.ads
      |-p_dms.adb
      |-p_commandfuncs.ads
      |-p_commandfuncs.adb
      \-main_sgf
```
<br>

## Types de données
***

Tous les types utilisés dans les programmes sont les types prédéfinis dans le langage en plus d'un type implanté (*unbounded_string*) ainsi que les types créés ci-après (*Arbre*, *Chemin* et *Commande*).\
Le type **unbounded_string** est une liste de caractères permettant donc de créer des chaines de caractères dynamiquement.
<br><br>

### Arbre
***

J'ai choisi l'arborescence ci-dessous pour le SGF lors de la conception, qui me semblait pratique. J'ai choisi cette dernière connaissant son efficacité algorithmique moindre, mais j'ai choisi de la garder car de mon point de vue elle changeait de ce que l'on pouvait voir avec les listes de pointeurs et était totalement différente de ce que l'on pouvait imaginer d'une arborescence SGF.

Le TAD de l'arbre se trouve dans le fichier **p_tree.ads**.

![Arbre](Arbre.png)

```ada
type Node;
type NodePtr is access Node;
type Node is record
   name : unbounded_string;
   extension : unbounded_string;
   rights : String(1..4);
   size : Integer;
   parentFolder : NodePtr;
   child : NodePtr;
   next : NodePtr;
end record;  
```

Le type **Node** est un enregistrement qui servira de représentation pour les fichiers et les répertoires. En effet les fichiers et les répertoires seront représentés par le même type, la différenciation entre les deux se fera au niveau des droits (**rights**) avec le premier caractère du champs **rights**, si le premier caractère est un **'-'** le *Noeu* sera un fichier alors que si c'est un **'d'** alors ce sera un dossier.
- **name** : le nom peut avoir une longueur illimitée
- **extension** : l'extension existe si le *Noeu* est un fichier, sinon l'extension est égale à **""**
- **rights** : les droits sont les mêmes que sous Unix excepté que nous ne prenons pas en compte les groupes, elles seront sous la forme "**x**rwx" avec x = '-' si c'est un fichier et x = 'd' si c'est un répertoire.
- **size** : la taille d'un fichier
- **parentFolder** : le pointeur qui va pointer sur le dossier parent
- **child** : le pointeur qui va pointer sur le premier fichier ou dossier enfant
- **next** : le pointeur qui va pointer sur le prochain répertoire/fichier de même niveau 
<br><br>

### Chemin (path)
***

Le type **R_Path** est utilisé lorsqu'un chemin est spécifié dans l'interpréteur de commande ou dans le menu. **R_Path** est un enregistrement de *unbounded_string*. Le fait d'avoir un pointeur dessus permet de créer une liste chainée permettant de retracer le chemin avec, pour chaque élément de la chaine un répertoire de l'arborescence. 

Le TAD du Chemin se trouve dans le fichier **p_tree.ads**.

```ada
type R_Path;
type StringPtr is access R_Path;
type R_Path is record
   name : unbounded_string;
   isAbsolute : Boolean;
   next : StringPtr;
end record;
```

- **name** : le nom du répertoire
- **isAbsolute** : si le chemin spécifié est un chemin absolu alors ce booléen sera à Vrai
- **next** : le pointeur qui va pointer sur le prochain élément de la liste
<br><br>

### Commande
***

Le type **R_Command** est utilisé afin de séparer les différents éléments d'une commande écrite dans l'interpréteur de commande. C'est un enregistrement de *unbounded_string*. Comme le type vu au-dessus c'est une liste chainée permettant de rendre la commande extensible.

Le TAD de la Commande se trouve dans le fichier **p_commandfuncs**.

```ada
type R_Command;
type CommandPtr is access R_Command;
type R_Command is record
   commandPart : unbounded_string;
   next : CommandPtr;
end record;
```

- **commandPart** : le nom de la partie de commande
- **next** : le pointeur qui va pointer sur le prochain élément de la liste
<br><br>

## Architecture de l'application en modules

### main_sgf
***

Le programme **main_sgf** est le coeur du SGF, c'est dans ce programme que les interactions avec l'utilisateur ainsi que le choix entre l'interpréteur de commande et le menu interactif se font. Le programme comporte 3 procedures : 
- *mainMenu*
- *commandMenu*
- *commandPrompt*
<br><br>

### p_tree
***

Le package **p_tree** est le package qui va permettre de construire toute l'arborescence du SGF, étant donné que le SGF va être sous forme d'arbre. Le package comporte multiples procédures et fonctions qui permettent d'initialiser, de rechercher, d'ajouter etc dans l'arbre. Tous les contrats de ce package se trouvent dans **p_tree.ads**.
- *createRootDMS*
- *initTree*
- *isEmptyTree*
- *changeCurrentTree*
- *addTree*
- *removeTree*
- *changeNodeName/Extension/Rights/Size*
- *getAbsoluteDir*
- *getLastPathPtr*
- *GetExtensionOfFile*
<br><br>

### p_commandfuncs
***

Le package **p_commandfuncs** est le package qui va permettre de faire des opérations sur les deux types de listes chainées défini ci-dessus : les listes chainées de *commande* et de *chemin*. Le package comporte de multiples procédures et fonctions qui permettent de diviser des *unbounded_string* par rapport à divers caractères mais aussi d'enlever et d'ajouter des éléments dans les listes chainées. Tous les contrats de ce package se trouvent dans **p_commandfuncs.ads**.
- *splitCommand*
- *splitPath*
- *insertCommand*
- *insertPath*
- *listChildLevel*
- *popLastString*
- *getLastString*
<br><br>

### p_dms
***

Le package **p_dms** est le package qui va permettre de réaliser les fonctions du SGF, c'est à dire par exemple *ls*, *cd* ou encore *mkdir*. Le package contient aussi l'interpréteur de commande utilisé dans le terminal. Tous les contrats de ce package se trouvent dans **p_dms.ads**
- *pwd*
- *cd*
- *ls*
- *mkdir*
- *touch*
- *rm*
- *vim*
- *help*
- *interpretCommand*
<br><br>

## Tests
***

N'ayant jamais fait de tests pour des programmes personnels réalisés avant ce projet, j'ai dû réfléchir comment un test pouvait être pertinent. J'ai donc décidé de tester toutes les fonctions et procédures des différents packages en prenant le plus de possibilités afin de ne pas oublier de cas qui pourraient faire douter la robustesse de mon **SGF**. De plus les tests m'ont permis de me rendre de compte de certains cas non pris en compte lors de la conception et de la réalisation du **SGF**

Tous les tests pour les 3 packages se trouvent dans le package **p_test** et ces derniers sont éxécutés dans le programme **test_main.adb**.

Tester tous les différentes procédures et fonctions des packages m'a permis de tester les fonctions racines ainsi que leur implémentation.
<br><br>

## Difficultés et Solutions
***

Lors de ce projet j'ai dû faire face à deux difficultés : 
- La première a été la conception de l'arbre, en effet les pointeurs étant un concept relativement nouveau pour moi il n'a pas été facile de les mettre en oeuvre au début. Mais en demandant de l'aide et en me renseignant j'ai réussi à comprendre ces concepts afin de réaliser une solution relativement différente de celle évoqué par mes camarades.
- La deuxième a été l'intégration des principes du langage. Ada est un langage très complet et surtout très verbeux, ce qui était différent des langages avec lesquels j'avais l'habitude de travailler. Il m'a donc fallu un certain temps d'adaption, mais qui en a value le coup car j'ai compris beaucoup d'éléments de la programmation grâce à ce langage.
<br><br>

## Bilan technique
***

Je pense avoir terminé la 1ère partie du projet à savoir réaliser un SGF avec certaines commandes de bases mais sans gérer toute la partie mémoire qui se trouve derrière. Mon manque de pratique dans la programmation résulte d'un code qui n'est peut-être pas le plus beau, mais qui se trouve être fonctionnel et surement compréhensible. Il y a donc une perspective d'amélioration dans la façon de coder et représenter les choses ainsi que dans l'implémentation d'autres fonctions.

Quant à la partie 2, j'aurais envisagé de coder la mémoire sur une liste doublement chainée qui serait donc extensible avec une taille limite de 1To comme précisé dans l'énoncé. De plus afin de représenter la mémoire le plus fidèlement possible, j'aurais fait un enregistrement comportement les niveaux de tailles (*octet*, *Ko*, *Mo*, *Go*, *To*).
<br><br>

## Bilan personnel
***

J'ai personnellement trouvé ce projet très interessant en tant que premier vrai projet dans la programmation. Il m'a permis d'apprendre beaucoup sur la façon de concevoir un programme *from Scratch* ainsi que de m'organiser sur quoi faire et quand le faire. De plus le sujet était très captivant vu que l'on représente quelque chose qui existe déjà et donc on peut éventuellement se mettre à la place de personnes qui pendant des années ont réfléchi sur ce système afin qu'il soit le plus beau, propre et fonctionnel possible, ce qui était donc aussi ma démarche.

J'ai passé pas mal de temps en dehors des heures prévues pour pouvoir réfléchir à des questions sur lesquelles j'avais du mal afin de pouvoir les poser à mes camarades ou à mes professeurs lors des séances en classe.

Je me suis rendu compte un peu tard que ma conception avait été un peu rapide au début du projet, c'est pour cela qu'après avoir codé les principaux concepts de mon *Arbre* j'ai décidé d'arrêter afin de reprendre ma conception et donc d'éclaircir des zones d'ombres, chose que je n'aurais pas fait avant ce projet.

Je trouve que le rapport représente beaucoup l'élève, c'est pour cela que je n'ai pas voulu pousser le projet, sinon je savais que mon rapport ne serait pas aussi bien que je le voudrais. J'ai passé beaucoup de temps également à rédiger mon rapport ainsi que mon manuel utilisateur afin qu'ils permettent d'illustrer mon implication dans ce projet très intéressant.

Ce projet a été pour moi une grande découverte, avec beaucoup de positif, tant en implémantation qu'en conception, j'ai pu apprendre beaucoup de concepts grâce au langage mais aussi en parlant avec mes camarades plus habitués à ce genre de projet. C'est pour cela que j'ai pris beaucoup de plaisir à réaliser ce projet.