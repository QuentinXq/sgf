with Ada.Strings.unbounded, Ada.Text_IO.Unbounded_IO, p_tree;
use Ada.Strings.unbounded, Ada.Text_IO.Unbounded_IO, p_tree;

package p_commandfuncs is

    -- (******************************** TAD ********************************)
    -- (* Liste chainée : Pointeur CommandPtr sur R_Command                 *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Types :                                                           *)
    -- (*       CommandPtr : Pointeur sur R_Command                         *)
    -- (*       R_Command : Enregistrement                                  *)
    -- (*           commandPart : unbounded_string, partie de la commande   *)
    -- (*           next : CommandPtr, Pointeur sur le prochain élément     *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Constructeurs :                                                   *)
    -- (*       insertCommand : Ajoute un nouvel élément à la liste         *)
    -- (*********************************************************************)
    type R_Command;
    type CommandPtr is access R_Command;
    type R_Command is record
                commandPart : unbounded_string;
                next : CommandPtr;
    end record;
    

    function splitCommand(command : in unbounded_string) return CommandPtr;
    -- (*********************************************************************)
    -- (* fonction splitCommand                                             *)
    -- (* sémantique :  permet de séparer la commande au niveau des ' '     *)
    -- (*               interprété dans le terminal et de la stocker dans   *)
    -- (*               une liste chainée                                   *)
    -- (* paramètre :                                                       *)
    -- (*       command : unbounded_string, commande du terminal            *)
    -- (* pré-conditions :  /                                               *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (* retour : CommandPtr, liste chaînée de partie de commande          *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_splitCommand                             *)

    function splitPath(path : in out unbounded_string) return StringPtr;
    -- (*********************************************************************)
    -- (* fonction splitPath                                                *)
    -- (* sémantique :  permet de séparer le chemin au niveau des '/'       *)
    -- (*               interprété dans une partie de la commande et de la  *)
    -- (*               stocker dans une liste chainée                      *)
    -- (* paramètre :                                                       *)
    -- (*       path : unbounded_string, chemin de la commande              *)
    -- (* pré-conditions :  /                                               *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (* retour : StringPtr, liste chaînée du chemin                       *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_splitPath                                *)

    procedure insertCommand(command : in unbounded_string; cmdPtr : in out CommandPtr);
    -- (*********************************************************************)
    -- (* procedure insertCommand                                           *)
    -- (* sémantique :  permet d'insérer un nouvelle élément dans la liste  *)
    -- (*               chainée en fonction de son nom (command)            *)
    -- (* paramètre :                                                       *)
    -- (*       command : unbounded_string, nom de la commande à ajouter    *)
    -- (*       cmdPtr : CommandPtr, liste chainée de commande              *)
    -- (* pré-conditions :  command non vide                                *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_insertCommand                            *)

    procedure insertPath(path : in out unbounded_string; pathPtr : in out StringPtr; isAbsolute : in Boolean);
    -- (*********************************************************************)
    -- (* procedure insertSplit                                             *)
    -- (* sémantique :  permet d'insérer un nouvelle élément dans la liste  *)
    -- (*               chainée en fonction de son nom (path) et de sa      *)
    -- (*               position                                            *)
    -- (* paramètre :                                                       *)
    -- (*       path : unbounded_string, nom du chemin à ajouter            *)
    -- (*       pathPtr : PathPtr, liste chainée de chemin                  *)
    -- (*       isAbsolute : Boolean, connaitre la relativité du chemin     *)
    -- (* pré-conditions :  path non vide                                   *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_insertPath                               *)

    procedure listChildLevel(currentTree : in NodePtr; option : in unbounded_string);
    -- (*********************************************************************)
    -- (* procedure listChildLevel                                          *)
    -- (* sémantique :  permet de lister les noms de tous les enfants du    *)
    -- (*               chemin courant                                      *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un chemin de l'arbre                 *)
    -- (*       option : unbounded_string, option d'affichage               *)
    -- (* pré-conditions :  /                                               *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_listChildLevel                           *)

    function popLastString(path : in out StringPtr) return StringPtr;
    -- (*********************************************************************)
    -- (* fonction popLastString                                            *)
    -- (* sémantique :  permet de retirer le dernier élément d'une liste    *)
    -- (*               chainée de chemin                                   *)
    -- (* paramètre :                                                       *)
    -- (*       path : StringPtr, liste chaînée de chemin                   *)
    -- (* pré-conditions :  /                                               *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (* retour : StringPtr, liste chaînée du chemin                       *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_popLastString                            *)

    function getLastString(path : in StringPtr) return unbounded_string;
    -- (*********************************************************************)
    -- (* fonction getLastString                                            *)
    -- (* sémantique :  permet de récupérer le dernier élément d'une liste  *)
    -- (*               chainée de chemin                                   *)
    -- (* paramètre :                                                       *)
    -- (*       path : StringPtr, liste chaînée de chemin                   *)
    -- (* pré-conditions :  /                                               *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (* retour : unbounded_string, dernier chemin de la liste             *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_getLastString                            *)

end p_commandfuncs;
