with Ada.Text_IO;
use Ada.Text_IO;

with Ada.Strings.unbounded;
use Ada.Strings.unbounded;

with Ada.Text_IO.Unbounded_IO;
use Ada.Strings.unbounded;

package body p_tree is
    function createRootDMS(currentTree : in out NodePtr) return NodePtr is
        newNode, anotherNode, anotherAgainNode : NodePtr;
    begin
        -- Création de tous les noeuds afin de ne pas avoir à les créers tout le temps représenté ci-dessous :
        -- /
        -- |-hello
        -- |   \-sup
        -- \-oi
        newNode := new Node'(To_Unbounded_String("/"),To_Unbounded_String(""),"drwx",0,null,null,null);
        currentTree := new Node'(To_Unbounded_String("hello"), To_Unbounded_String(""),"drwx",0,newNode,null,null);
        anotherNode := new Node'(To_Unbounded_String("sup"), To_Unbounded_String(""),"drwx",0,currentTree,null,null);
        anotherAgainNode := new Node'(To_Unbounded_String("oi"), To_Unbounded_String(""),"drwx",0,newNode,null,null);
        newNode.all.child := currentTree;
        currentTree.all.next := anotherAgainNode;
        currentTree.all.child := anotherNode;
        return currentTree;
    end createRootDMS;

    procedure initTree(currentTree : out NodePtr) is
    begin
        -- Initialise l'arbre à null
        currentTree := null;
    end initTree;

    function isEmptyTree(currentTree : in NodePtr) return Boolean is
    begin
        -- Retourne si l'arbre est null
        return currentTree = null;
    end isEmptyTree;

    function changeCurrentTree(currentTree : in out NodePtr; path : in out StringPtr) return NodePtr is
        currentTreeCopy : NodePtr;
        isIn : Boolean := False;
    begin
        -- Variable permmettant de ne pas appliquer les modifications sur la variable globale currentTree
        currentTreeCopy := currentTree;
        -- On regarde si l'arbre est null
        if currentTreeCopy /= null then
            -- On regarde ensuite si le premier élément du chemin est un '/' qui signifierai que c'est un chemin absolu
            if path.all.name = To_Unbounded_String("/") then
                -- On regarde si l'élément après '/' éxiste et on prend sa valeur
                if path.all.next /= null then
                    path := path.all.next;
                end if;
                -- On sait que c'est un chemin absolu donc on remonte jusqu'à la racine
                while currentTreeCopy.all.parentFolder /= null loop
                    currentTreeCopy := currentTreeCopy.all.parentFolder;
                end loop;
            end if;
            -- On regarde si le premier élément est '..' ce qui correspond à un retour vers le parent
            if path.all.name = To_Unbounded_String("..") then
                -- On regarde si le parent est pas null car sinon on ne pourrait pas remonter vers lui étant null
                if currentTreeCopy.all.parentFolder /= null then
                    currentTreeCopy := currentTreeCopy.all.parentFolder;
                    -- On regarde si l'élément après '..' éxiste et on prend sa valeur
                    if path.all.next /= null then
                        path := path.all.next;
                    -- Sinon on retourne le Noeud du parent
                    else
                        return currentTreeCopy;
                    end if;
                end if;
            end if;
            -- On regarde si le premier élément est '.' car cela correspond au répertoire courant
            if path.all.name = To_Unbounded_String(".") then
                -- On regarde si l'élément après '.' éxiste et on prend sa valeur
                if path.all.next /= null then
                    path := path.all.next;
                -- Sinon on retourne le répertoire courant
                else
                    return currentTreeCopy;
                end if;
            end if;
            -- On regarde si le prochain élément du chemin existe et si nom de l'élément actuel n'est pas la racine
            if path.all.next /= null or path.all.name /= To_Unbounded_String("/") then
                -- On descend au niveau enfant
                currentTreeCopy := currentTreeCopy.all.child;
            end if;
            -- On regarde si le chemin est le dernier élément du chemin complet puis si le nom du Noeud courant est différent du nom du chemin
            if path.all.next = null and then currentTreeCopy.all.name /= path.all.name then
                -- On boucle tant que le prochain Noeud de même niveau est null et puis tant que le nom du Noeud n'est pas le même nom que celui du chemin
                while currentTreeCopy.all.next /= null and then currentTreeCopy.all.name /= path.all.name loop
                    currentTreeCopy := currentTreeCopy.all.next;
                    -- On regarde si le nom du Noeud est le même que celui du chemin et si le Noeud est bien un dossier et non un fichier
                    if currentTreeCopy.all.name = path.all.name and currentTreeCopy.all.rights(1) = 'd' then
                        -- On affecte à un Boolean True pour que l'on puisse s'en souvenir
                        isIn := True;
                    end if;
                end loop;
                -- Si le Boolean est à True alors on a trouvé le Noeud correspondant au chemin
                if isIn = True then
                    return currentTreeCopy;
                -- Sinon on retourne le Noeud courrant définit au départ et on indique que le dossier recherché n'existe pas
                else
                    Put_line("oops, folder not found");
                    return currentTree;
                end if;
            end if;
            -- On boucle tant que le prochain élément du chemin complet n'est pas null
            while path.all.next /= null loop
                -- On boucle tant que le Noeud est un dossier et tant que le nom du Noeud est différent de celui du chemin
                while currentTreeCopy.all.rights(1) = 'd' and then currentTreeCopy.all.name /= path.all.name loop
                    currentTreeCopy := currentTreeCopy.all.next;
                end loop;
                -- On regarde si le nom du Noeud est le même que celui du chemin et si le Noeud est bien un dossier
                if currentTreeCopy.all.name = path.all.name and currentTreeCopy.all.rights(1) = 'd' then
                    -- On descend à l'enfant
                    currentTreeCopy := currentTreeCopy.all.child;
                    -- Vu qu'on descend à l'enfant on a franchi une étape du chemin complet on passe donc au prochain élément du chemin complet
                    path := path.all.next;
                end if;
            end loop;
            -- On regarde si le Noeud actuel à le même nom que le chemin
            if currentTreeCopy.all.name = path.all.name then
                return currentTreeCopy;
            -- Sinon on retourne le Noeud d'origine et on indique que le Noeud n'existe pas
            else
                Put_line("oops, folder not found");
                return currentTree;
            end if;
        -- Sinon on retourne null et on indique que le Noeud spécifié est null
        else
            Put_line("Current Directory null");
            return null;
        end if;
    end changeCurrentTree;


    procedure addTree(currentTree : in out NodePtr; name : in unbounded_string; dirFile : in String) is
        newNode : NodePtr;
        extension : unbounded_string;
        currentTreeCopy : NodePtr;
    begin
        -- Variable permmettant de ne pas appliquer les modifications sur la variable globale currentTree
        currentTreeCopy := currentTree;
        -- Permet d'avoir l'extension du nom indiqué si elle existe
        extension := getExtensionOfFile(name);
        -- On regarde si le Noeud courant à un enfant
        if currentTreeCopy.all.child /= null then
            -- On va à l'enfant
            currentTreeCopy := currentTreeCopy.all.child;
            -- On regarde si le prochain Noeud de même niveau est vide
            if currentTreeCopy.all.next = null then
                -- On créé le nouveau Noeud et on indique que le nouveau Noeud est le suivant du précédent
                newNode := new Node'(name, extension, dirFile,0, currentTreeCopy.all.parentFolder, null,null);
                currentTreeCopy.all.next := newNode;
            else
                -- On boucle tant que le Noeud prochain est vide
                while currentTreeCopy.all.next /= null loop
                    currentTreeCopy := currentTreeCopy.all.next;
                end loop;
                -- On créé le nouveau Noeud et on indique que le nouveau Noeud est le suivant du précédent
                newNode := new Node'(name, extension, dirFile,0, currentTreeCopy.all.parentFolder, null,null);
                currentTreeCopy.all.next := newNode;
            end if;
        -- Sinon le Noeud n'a pas d'enfant donc on ajoute le nouveau Noeud comme enfant du Noeud courant
        else
            newNode := new Node'(name, extension, dirFile,0, currentTreeCopy, null,null);
            currentTreeCopy.all.child := newNode;
        end if;
    end addTree;

    procedure removeTree(currentTree : in out NodePtr; name : in unbounded_string; dirFile : in unbounded_string) is
        currentTreeCopy : NodePtr;
    begin
        -- Variable permmettant de ne pas appliquer les modifications sur la variable globale currentTree
        currentTreeCopy := currentTree;
        -- On regarde si l'enfant du Noeud courant n'est pas null
        if currentTreeCopy.all.child /= null then
            -- On regarde si le nom du Noeud enfant est le même que celui du Noeud à supprimer (name) et si le Noeud enfant est bien de la même nature (dossier/fichier) que la variable dirFile
            if currentTreeCopy.all.child.all.name = name and currentTreeCopy.all.child.all.rights(1) = Element(dirFile, 1) then
                -- On regarde si le Noeud enfant n'a pas un prochain Noeud de même niveau
                if currentTreeCopy.all.child.all.next = null then
                    -- Le Noeud enfant est supprimer
                    currentTreeCopy.all.child := null;
                -- Sinon le Noeud suivant du Noeud enfant devient le Noeud enfant
                else
                    currentTreeCopy.all.child := currentTreeCopy.all.child.all.next;
                end if;
            else
                -- Le Noeud enfant devient le Noeud courant (on passe au niveau enfant)
                currentTreeCopy := currentTreeCopy.all.child;
                -- On boucle tant que le Noeud courant n'est pas null
                while currentTreeCopy /= null loop
                    -- On regarde si le prochain Noeud n'est pas null
                    if currentTreeCopy.all.next /= null then
                        -- On regarde le nom du prochain Noeud est le même que le nom du Noeud à supprimer et si le Noeud enfant est bien de la même nature (dossier/fichier) que la variable dirFile
                        if currentTreeCopy.all.next.all.name = name and currentTreeCopy.all.next.all.rights(1) = Element(dirFile, 1) then
                            -- Le Noeud courant prend comme prochain Noeud le prochain prochain Noeud
                            currentTreeCopy.all.next := currentTreeCopy.all.next.all.next;
                        end if;
                    end if;
                    -- On regarde si le prochain Noeud du Noeud courant est null et si le nom du Noeud courant correspond au nom du Noeud à supprimer et si le Noeud enfant est bien de la même nature (dossier/fichier) que la variable dirFile
                    if currentTreeCopy.all.next = null and currentTreeCopy.all.name = name and currentTreeCopy.all.rights(1) = Element(dirFile, 1) then
                        -- On supprimer le Noeud courant
                        currentTreeCopy := null;
                    end if;
                currentTreeCopy := currentTreeCopy.all.next;
            end loop;
            end if;
        end if;
    end removeTree;


    procedure changeNodeName(currentTree: in out NodePtr; name : in unbounded_string) is
    begin
        -- On affecte le nom en paramètre au nom du Noeud
        currentTree.all.name := name;
    end changeNodeName;


    procedure changeNodeExtension(currentTree: in out NodePtr; extension : in unbounded_string) is
    begin
        -- On affecte l'extension en paramètre à l'extension du Noeud
        currentTree.all.extension := extension;
    end changeNodeExtension;


    procedure changeNodeRights(currentTree: in out NodePtr; rights : in String) is
    begin
        -- On affecte les droits en paramètre aux droits du Noeud
        currentTree.all.rights := rights;
    end changeNodeRights;


    procedure changeNodeSize(currentTree: in out NodePtr; size : in Integer) is
    begin
        -- On affecte la taille en paramètre la taille du Noeud
        currentTree.all.size := size;
    end changeNodeSize;

    function getAbsoluteDir(currentTree : in NodePtr) return unbounded_string is
        currentTreeCopy : NodePtr;
        path : unbounded_string;
        tmp_string : unbounded_string;
    begin
        -- Variable permmettant de ne pas appliquer les modifications sur la variable globale currentTree
        currentTreeCopy := currentTree;
        -- On ajoute un '/' à la fin du chemin
        path := currentTreeCopy.all.name & '/';
        -- On boucle tant que le parent du Noeud courant n'est pas null
        while currentTreeCopy.all.parentFolder /= null loop
            -- On va au Noeud parent et on ajoute le nom du Noeud au chemin
            currentTreeCopy := currentTreeCopy.all.parentFolder;
            tmp_string := currentTreeCopy.all.name & '/';
            path := tmp_string & path;
        end loop;
        -- On regarde si le nom du Noeud courant est le Noeud racine
        if currentTree.all.name = To_Unbounded_String("/") then
            -- On supprime le dernier caractère du chemin car il est en doublon
            Delete(path, Length(path), Length(path));
            return path;
        else
            Delete(path, 1, 1);
            return path;
        end if;
    end getAbsoluteDir;


    function getLastPathPtr(path : in out StringPtr) return StringPtr is
    begin
        -- On boucle tant que le prochain élément de la liste n'est pas null
        while path.all.next /= null loop
            path := path.all.next;
        end loop;
        -- On retourne le dernier élément de la liste
        return path;
    end getLastPathPtr;
    

    function getExtensionOfFile(name : in unbounded_string) return unbounded_string is
        extension : unbounded_string;
        count : Integer := 1;
    begin
        -- On boucle tant que l'on ne rencontre pas un '.' dans le nom et tant que l'on ne dépasse pas la taille du nom
        while Element(name, count) /= '.' and count < Length(name) loop
            count := count +1;
        end loop;
        -- On regarde si le nom n'est pas vide et si le compteur est différent de la longueur du nom
        if count /= 1 and count /= Length(name) then
            -- On récupère les caractères après le '.'
            Unbounded_Slice(name, extension, count, Length(name));
            return extension;
        -- Sinon on retourne une chaine de caractères
        else
            return To_Unbounded_String("");
        end if;
    end getExtensionOfFile;

end p_tree;