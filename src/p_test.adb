with p_tree, p_commandfuncs, p_dms, Ada.Strings.unbounded, Ada.Text_IO.Unbounded_IO;
use p_tree, p_commandfuncs, p_dms, Ada.Strings.unbounded, Ada.Text_IO.Unbounded_IO;

with Ada.Text_IO;
use Ada.Text_IO;

package body p_test is
    currentTree, newNode, anotherNode, anotherAgainNode, anotherAgainAgainNode : NodePtr;

    procedure initTreeForTest is
    begin
        newNode := new Node'(To_Unbounded_String("/"),To_Unbounded_String(""),"drwx",0,null,null,null);
        currentTree := new Node'(To_Unbounded_String("hello"), To_Unbounded_String(""),"drwx",0,newNode,null,null);
        anotherNode := new Node'(To_Unbounded_String("sup"), To_Unbounded_String(""),"drwx",0,currentTree,null,null);
        anotherAgainNode := new Node'(To_Unbounded_String("oi"), To_Unbounded_String(""),"drwx",0,newNode,null,null);
        anotherAgainAgainNode := new Node'(To_Unbounded_String("test.txt"), To_Unbounded_String(".txt"),"-rwx",0,anotherAgainNode,null,null);
        newNode.all.child := currentTree;
        currentTree.all.next := anotherAgainNode;
        currentTree.all.child := anotherNode;
        anotherAgainNode.all.child := anotherAgainAgainNode;
    end initTreeForTest;

    -- ########## TESTS FOR PACKAGE P_TREE ##########
    procedure test_createRootDMS is
    begin
        Put_line("##### BEGIN TEST_CREATEROOTDMS #####");
        currentTree := createRootDMS(currentTree);
        if currentTree /= null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_CREATEROOTDMS #####");
    end test_createRootDMS;

    procedure test_initTree is
    begin
        Put_line("##### BEGIN TEST_INITTREE #####");
        initTree(currentTree);
        if currentTree = null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_INITTREE #####");
    end test_initTree;

    procedure test_isEmptyTree is
    begin
        Put_line("##### BEGIN TEST_ISEMPTYTREE #####");
        -- First test
        Put_line("## FIRST TEST ##");
        currentTree := null;
        if isEmptyTree(currentTree) then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Second test
        Put_line("## SECOND TEST ##");
        currentTree := new Node'(To_Unbounded_String("/"),To_Unbounded_String(""),"drwx",0,null,null,null);
        if isEmptyTree(currentTree) then
            Put_line("TEST NOT OK");
        else
            Put_line("TEST OK");
        end if;
        Put_line("##### END TEST_ISEMPTYTREE #####");
    end test_isEmptyTree;

    procedure test_changeCurrentTree is
        currentTreeCopy : NodePtr;
        path : unbounded_string;
        pathPtr : StringPtr;
    begin
        Put_line("##### BEGIN TEST_CHANGECURRENTTREE #####");
        -- First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        currentTreeCopy := currentTree;
        path := To_Unbounded_String(".");
        pathPtr := splitPath(path);
        if changeCurrentTree(currentTreeCopy, pathPtr).all.name = currentTree.all.name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("..");
        pathPtr := splitPath(path);
        if changeCurrentTree(currentTreeCopy, pathPtr).all.name = currentTree.all.parentFolder.all.name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("/");
        pathPtr := splitPath(path);
        if changeCurrentTree(currentTreeCopy, pathPtr).all.name = currentTree.all.parentFolder.all.name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Fourth test
        Put_line("## FOURTH TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("sup");
        pathPtr := splitPath(path);
        if changeCurrentTree(currentTreeCopy, pathPtr).all.name = currentTree.all.child.all.name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Fifth test
        Put_line("## FIFTH TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("/hello/sup");
        pathPtr := splitPath(path);
        if changeCurrentTree(currentTreeCopy, pathPtr).all.name = currentTree.all.child.all.name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Sixth test
        Put_line("## SIXTH TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("../hello");
        pathPtr := splitPath(path);
        if changeCurrentTree(currentTreeCopy, pathPtr).all.name = currentTree.all.name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_CHANGECURRENTTREE #####");
    end test_changeCurrentTree;

    procedure test_addTree is
        name : unbounded_string;
        dirFile : String(1..4) := "drwx";
    begin
        Put_line("##### BEGIN TEST_ADDTREE #####");
        name := To_Unbounded_String("salut");
        -- First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        addTree(anotherNode, name, dirFile);
        if anotherNode.all.child.all.name = name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        addTree(currentTree, name, dirFile);
        if currentTree.all.child.all.next.all.name = name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_ADDTREE #####");
    end test_addTree;

    procedure test_removeTree is
        name : unbounded_string;
        dirFile : unbounded_string;
    begin
        Put_line("##### BEGIN TEST_REMOVETREE #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        name := To_Unbounded_String("sup");
        dirFile := To_Unbounded_String("d");
        removeTree(currentTree, name, dirFile);
        if currentTree.all.child = null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        name := To_Unbounded_String("sup");
        dirFile := To_Unbounded_String("-");
        removeTree(currentTree, name, dirFile);
        if currentTree.all.child /= null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        name := To_Unbounded_String("oi");
        dirFile := To_Unbounded_String("d");
        removeTree(newNode, name, dirFile);
        if newNode.all.child.all.next = null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Fourth test
        Put_line("## FOURTH TEST ##");
        initTreeForTest;
        name := To_Unbounded_String("oi");
        dirFile := To_Unbounded_String("-");
        removeTree(newNode, name, dirFile);
        if newNode.all.child.all.next /= null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_REMOVETREE #####");
    end test_removeTree;

    procedure test_changeNodeName is
        name : unbounded_string;
    begin
        Put_line("##### BEGIN TEST_CHANGENODENAME #####");
        name := To_Unbounded_String("test");
        initTreeForTest;
        changeNodeName(currentTree, name);
        if currentTree.all.name = name then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_CHANGENODENAME #####");
    end test_changeNodeName;

    procedure test_changeNodeRights is
        rights : String(1..4) := "dr-x";
    begin
        Put_line("##### BEGIN TEST_CHANGENODERIGHTS #####");
        initTreeForTest;
        changeNodeRights(currentTree, rights);
        if currentTree.all.rights = rights then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_CHANGENODERIGHTS #####");
    end test_changeNodeRights;

    procedure test_changeNodeExtension is
        extension : unbounded_string;
    begin
        Put_line("##### BEGIN TEST_CHANGENODEEXTENSION #####");
        initTreeForTest;
        changeNodeExtension(currentTree, extension);
        if currentTree.all.extension = extension then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_CHANGENODEEXTENSION #####");
    end test_changeNodeExtension;

    procedure test_changeNodeSize is
        size : Integer := 7;
    begin
        Put_line("##### BEGIN TEST_CHANGENODESIZE #####");
        initTreeForTest;
        changeNodeSize(currentTree, size);
        if currentTree.all.size = size then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_CHANGENODESIZE #####");
    end test_changeNodeSize;

    procedure test_getAbsoluteDir is
        newNode : NodePtr;
    begin
        Put_line("##### BEGIN TEST_GETABSOLUTEDIR #####");
        newNode := new Node'(To_Unbounded_String("/"),To_Unbounded_String(""),"drwx",0,null,null,null);
        currentTree := new Node'(To_Unbounded_String("hello"), To_Unbounded_String(""),"drwx",0,newNode,null,null);
        newNode.all.child := currentTree;
        if getAbsoluteDir(currentTree) = To_Unbounded_String("/hello/") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_GETABSOLUTEDIR #####");
    end test_getAbsoluteDir;

    procedure test_getLastPathPtr is
        pathPtr : StringPtr;
        path : unbounded_string;
    begin
        Put_line("##### BEGIN TEST_GETLASTPATH #####");
        path := To_Unbounded_String("/hello/toGet");
        pathPtr := splitPath(path);
        if getLastPathPtr(pathPtr).all.name = To_Unbounded_String("toGet") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if; 
        Put_line("##### END TEST_GETLASTPATH #####");
    end test_getLastPathPtr;

    procedure test_getExtensionOfFile is
        name : unbounded_string;
    begin
        Put_line("##### BEGIN TEST_GETEXTENSIONOFFILE #####");
        name := To_Unbounded_String("test.txt");
        if getExtensionOfFile(name) = To_Unbounded_String(".txt") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_GETEXTENSIONOFFILE #####");
    end test_getExtensionOfFile;
    -- ########## TESTS FOR PACKAGE P_TREE ##########

        -- ########## TESTS FOR PACKAGE P_COMMANDFUNCS ##########
    procedure test_splitCommand is
    command : unbounded_string;
    cmdPtr : commandPtr;
    begin
        Put_line("##### BEGIN TEST_SPLITCOMMAND #####");
        -- First test
        Put_line("## FIRST TEST ##");
        command := To_Unbounded_String("cd /hello");
        cmdPtr := splitCommand(command);
        if cmdPtr.all.commandPart = To_Unbounded_String("cd") and cmdPtr.all.next.all.commandPart = To_Unbounded_String("/hello") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Second test
        Put_line("## SECOND TEST ##");
        command := To_Unbounded_String("ls");
        cmdPtr := splitCommand(command);
        if cmdPtr.all.commandPart = To_Unbounded_String("ls") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Third test
        Put_line("## THIRD TEST ##");
        command := To_Unbounded_String("");
        cmdPtr := splitCommand(command);
        if cmdPtr.all.commandPart = To_Unbounded_String("") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_SPLITCOMMAND #####");
    end test_splitCommand;

    procedure test_splitPath is
        path : unbounded_string;
        pathPtr : StringPtr;
    begin
        Put_line("##### BEGIN TEST_SPLITPATH #####");
        -- First test
        Put_line("## FIRST TEST ##");
        path := To_Unbounded_String("/hello");
        pathPtr := splitPath(path);
        if pathPtr.all.name = To_Unbounded_String("/") and pathPtr.all.next.all.name = To_Unbounded_String("hello") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Second test
        Put_line("## SECOND TEST ##");
        path := To_Unbounded_String("/");
        pathPtr := splitPath(path);
        if pathPtr.all.name = To_Unbounded_String("/") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Third test
        Put_line("## THIRD TEST ##");
        path := To_Unbounded_String("");
        pathPtr := splitPath(path);
        if pathPtr.all.name = To_Unbounded_String(".") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_SPLITPATH #####");
    end test_splitPath;

    procedure test_insertCommand is
        command : unbounded_string;
        cmdPtr : commandPtr;
    begin
        Put_line("##### BEGIN TEST_INSERTCOMMAND #####");
        -- First test
        Put_line("## FIRST TEST ##");
        command := To_Unbounded_String("ls");
        cmdPtr := null;
        insertCommand(command, cmdPtr);
        if cmdPtr.all.commandPart = command then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Second test
        Put_line("## SECOND TEST ##");
        command := To_Unbounded_String("ls");
        cmdPtr := new R_Command'(To_Unbounded_String("test"), null);
        insertCommand(command, cmdPtr);
        if cmdPtr.all.commandPart = To_Unbounded_String("test") and cmdPtr.all.next.all.commandPart = command then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_INSERTCOMMAND #####");
    end test_insertCommand;

    procedure test_insertPath is
        path : unbounded_string;
        pathPtr : StringPtr;
    begin
        Put_line("##### BEGIN TEST_INSERTPATH #####");
        -- First test
        Put_line("## FIRST TEST ##");
        path := To_Unbounded_String("/");
        pathPtr := null;
        insertPath(path, pathPtr, True);
        if pathPtr.all.name = path then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        -- Second test
        Put_line("## SECOND TEST ##");
        path := To_Unbounded_String("hello");
        pathPtr := new R_Path'(To_Unbounded_String("/"),True , null);
        insertPath(path, pathPtr, False);
        if pathPtr.all.name = To_Unbounded_String("/") and pathPtr.all.next.all.name = path then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_INSERTPATH #####");
    end test_insertPath;

    procedure test_listChildLevel is
        option : unbounded_string;
    begin
        Put_line("##### BEGIN TEST_LISTCHILDLEVEL #####");
        --First test
        Put_line("## FIRST TEST ##");
        option := To_Unbounded_String("");
        initTreeForTest;
        Put_line("TEST OK : sup");
        listChildLevel(currentTree, option);

        --Second test
        Put_line("## SECOND TEST ##");
        option := To_Unbounded_String("-l");
        initTreeForTest;
        Put_line("TEST OK : drwx      0  sup");
        listChildLevel(currentTree, option);

        Put_line("##### END TEST_LISTCHILDLEVEL #####");
    end test_listChildLevel;

    procedure test_popLastString is
        path : StringPtr;
    begin
        Put_line("##### BEGIN TEST_POPLASTSTRING #####");
        --First test
        Put_line("## FIRST TEST ##");
        path := null;
        path := popLastString(path);
        if path = null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Second test
        Put_line("## SECOND TEST ##");
        path := new R_Path'(To_Unbounded_String("/"), True, null);
        path := popLastString(path);
        if path = null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_POPLASTSTRING #####");
    end test_popLastString;

    procedure test_getLastString is
        path : unbounded_string;
        pathPtr : StringPtr;
    begin
        Put_line("##### BEGIN TEST_GETLASTSTRING #####");
        --First test
        Put_line("## FIRST TEST ##");
        path := To_Unbounded_String("/hello/oi");
        pathPtr := splitPath(path);
        if getLastString(pathPtr) = To_Unbounded_String("oi") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Second test
        Put_line("## SECOND TEST ##");
        path := To_Unbounded_String("/");
        pathPtr := splitPath(path);
        if getLastString(pathPtr) = To_Unbounded_String("/") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Third test
        Put_line("## THIRD TEST ##");
        pathPtr := null;
        if getLastString(pathPtr) = To_Unbounded_String("") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_GETLASTSTRING #####");
    end test_getLastString;
    -- ########## TESTS FOR PACKAGE P_COMMANDFUNCS ##########

    -- ########## TESTS FOR PACKAGE P_DMS ##########
    procedure test_pwd is
        aTree : NodePtr;
    begin
        Put_line("##### BEGIN TEST_PWD #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        Put_line("TEST OK : /hello/");
        pwd(currentTree);

        --Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        Put_line("TEST OK : /");
        pwd(newNode);

        --Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        Put_line("TEST OK : /hello/sup/");
        pwd(anotherNode);

        --Fourth test
        Put_line("## FOURTH TEST ##");
        aTree := null;
        Put_line("TEST OK : Current Directory null");
        pwd(aTree);
        Put_line("##### END TEST_PWD #####");
    end test_pwd;

    procedure test_cd is
        path : unbounded_string;
        pathPtr : StringPtr;
        aTree : NodePtr;
    begin
        Put_line("##### BEGIN TEST_CD #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("/");
        pathPtr := splitPath(path);
        if cd(currentTree, pathPtr).all.name = To_Unbounded_String("/") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("..");
        pathPtr := splitPath(path);
        if cd(currentTree, pathPtr).all.name = To_Unbounded_String("/") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        path := To_Unbounded_String(".");
        pathPtr := splitPath(path);
        if cd(currentTree, pathPtr).all.name = To_Unbounded_String("hello") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Fourth test
        Put_line("## FOURTH TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("../oi");
        pathPtr := splitPath(path);
        if cd(currentTree, pathPtr).all.name = To_Unbounded_String("oi") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Fifth test
        Put_line("## FIFTH TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("sup");
        pathPtr := splitPath(path);
        if cd(currentTree, pathPtr).all.name = To_Unbounded_String("sup") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Sixth test
        Put_line("## SIXTH TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("/hello/sup");
        pathPtr := splitPath(path);
        if cd(currentTree, pathPtr).all.name = To_Unbounded_String("sup") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Seventh test
        Put_line("## SEVENTH TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("hello/sup");
        pathPtr := splitPath(path);
        if cd(newNode, pathPtr).all.name = To_Unbounded_String("sup") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Eigth test
        Put_line("## EIGTH TEST ##");
        aTree := null;
        path := To_Unbounded_String("/");
        pathPtr := splitPath(path);
        if cd(aTree, pathPtr) = null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Ninth test
        Put_line("## NINTH TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("");
        pathPtr := splitPath(path);
        if cd(currentTree, pathPtr).all.name = To_Unbounded_String("hello") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_CD #####");
    end test_cd;

    procedure test_ls is
        option : unbounded_string;
        path : unbounded_string;
        pathPtr : StringPtr;
    begin
        Put_line("##### BEGIN TEST_LS #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("");
        path := To_Unbounded_String("");
        pathPtr := null;
        Put_line("TEST OK : sup");
        ls(currentTree, pathPtr, option);

        --Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("");
        path := To_Unbounded_String("/");
        pathPtr := splitPath(path);
        Put_line("TEST OK : hello oi");
        ls(currentTree, pathPtr, option);

        --Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("");
        path := To_Unbounded_String("/oi");
        pathPtr := splitPath(path);
        Put_line("TEST OK : test.txt");
        ls(currentTree, pathPtr, option);

        --Fourth test
        Put_line("## FOURTH TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("-l");
        path := To_Unbounded_String("/");
        pathPtr := splitPath(path);
        Put_line("TEST OK : drwx        0  hello");
        Put_line("          drwx        0  oi");
        ls(currentTree, pathPtr, option);

        --Fifth test
        Put_line("## FIFTH TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("-l");
        path := To_Unbounded_String("");
        pathPtr := null;
        Put_line("TEST OK : drwx        0  sup");
        ls(currentTree, pathPtr, option);
        Put_line("##### END TEST_LS #####");
    end test_ls;

    procedure test_mkdir is
        path : unbounded_string;
        pathPtr : StringPtr;
    begin
        Put_line("##### BEGIN TEST_MKDIR #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("test");
        pathPtr:= splitPath(path);
        mkdir(currentTree, pathPtr);
        if currentTree.all.child.all.next.all.name = path then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        pathPtr:= null;
        Put_line("TEST OK : missing argument(s)");
        mkdir(currentTree, pathPtr);
        
        --Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("/hello/sup/test");
        pathPtr:= splitPath(path);
        mkdir(currentTree, pathPtr);
        if currentTree.all.child.all.child.all.name = To_Unbounded_String("test") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_MKDIR #####");
    end test_mkdir;

    procedure test_touch is
        path : unbounded_string;
        pathPtr : StringPtr;
    begin
        Put_line("##### BEGIN TEST_TOUCH #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("test.txt");
        pathPtr:= splitPath(path);
        touch(currentTree, pathPtr);
        if currentTree.all.child.all.next.all.name = path then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        pathPtr:= null;
        Put_line("TEST OK : missing argument(s)");
        touch(currentTree, pathPtr);
        
        --Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("/hello/sup/test.txt");
        pathPtr:= splitPath(path);
        touch(currentTree, pathPtr);
        if currentTree.all.child.all.child.all.name = To_Unbounded_String("test.txt") then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_TOUCH #####");
    end test_touch;

    procedure test_rm is
        path : unbounded_string;
        pathPtr : StringPtr;
        option : unbounded_string;
    begin
        Put_line("##### BEGIN TEST_RM #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("");
        path := To_Unbounded_String("sup");
        pathPtr := splitPath(path);
        rm(currentTree, pathPtr, option);
        if currentTree.all.child.all.name = path  then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("-r");
        path := To_Unbounded_String("sup");
        pathPtr := splitPath(path);
        rm(currentTree, pathPtr, option);
        if currentTree.all.child = null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("");
        path := To_Unbounded_String("test.txt");
        pathPtr := splitPath(path);
        rm(anotherAgainNode, pathPtr, option);
        if anotherAgainNode.all.child = null then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Fourth test
        Put_line("## FOURTH TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("-r");
        path := To_Unbounded_String("test.txt");
        pathPtr := splitPath(path);
        rm(anotherAgainNode, pathPtr, option);
        if anotherAgainNode.all.child.all.name = path then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Fifth test
        Put_line("## FIFTH TEST ##");
        initTreeForTest;
        option := To_Unbounded_String("-r");
        pathPtr := null;
        Put_line("TEST OK : missing argument(s)");
        rm(anotherAgainNode, pathPtr, option);
        Put_line("##### END TEST_RM #####");
    end test_rm;

    procedure test_vim is
        path : unbounded_string;
        pathPtr : StringPtr;
        size : Integer;
    begin
        Put_line("##### BEGIN TEST_VIM #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("/oi/test.txt");
        pathPtr := splitPath(path);
        size := 45;
        vim(currentTree, pathPtr, size);
        if anotherAgainAgainNode.all.size = size then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;

        --Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        pathPtr := null;
        size := 45;
        Put_line("TEST OK : missing argument(s)");
        vim(currentTree, pathPtr, size);

        --Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        path := To_Unbounded_String("/oi/test.txt");
        pathPtr := splitPath(path);
        size := 0;
        vim(currentTree, pathPtr, size);
        if anotherAgainAgainNode.all.size = size then
            Put_line("TEST OK");
        else
            Put_line("TEST NOT OK");
        end if;
        Put_line("##### END TEST_VIM #####");
    end test_vim;

    procedure test_help is
    begin
        Put_line("##### BEGIN TEST_HELP #####");
        Put_line("TEST OK : cd          Change directory  ");
        Put_Line("          help        Display commands");
        Put_Line("          ls          Display folder(s) and file(s)");
        Put_Line("          mkdir       Create a folder");
        Put_Line("          pwd         Display the current directory");
        Put_Line("          rm          Remove folder or file");
        Put_Line("          touch       Create a file");
        Put_Line("          vim         Change size of the file");
        help;
        Put_line("##### END TEST_HELP #####");
    end test_help;

    procedure test_interpretCommand is
        command : unbounded_string;
        cmdPtr : CommandPtr;
    begin
        Put_line("##### BEGIN TEST_INTERPRETCOMMAND #####");
        --First test
        Put_line("## FIRST TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("pwd");
        cmdPtr := splitCommand(command);
        Put_line("TEST OK : /hello/");
        interpretCommand(cmdPtr, currentTree);

        --Second test
        Put_line("## SECOND TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("cd /");
        cmdPtr := splitCommand(command);
        interpretCommand(cmdPtr, currentTree);
        if currentTree.all.name = To_Unbounded_String("/") then
            Put_Line("TEST OK");
        else
            Put_Line("TEST NOT OK");
        end if;

        --Third test
        Put_line("## THIRD TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("ls -l /");
        cmdPtr := splitCommand(command);
        Put_Line("TEST OK : drwx        0  hello");
        Put_Line("          drwx        0  oi");
        interpretCommand(cmdPtr, currentTree);
        
        --Fourth test
        Put_line("## FOURTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("ls /");
        cmdPtr := splitCommand(command);
        Put_Line("TEST OK : hello oi");
        interpretCommand(cmdPtr, currentTree);
        
        --Fifth test
        Put_line("## FIFTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("mkdir test");
        cmdPtr := splitCommand(command);
        interpretCommand(cmdPtr, currentTree);
        if currentTree.all.child.all.next.all.name = To_Unbounded_String("test") then
            Put_Line("TEST OK");
        else
            Put_Line("TEST NOT OK");
        end if;

        --Sixth test
        Put_line("## SIXTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("touch test.txt");
        cmdPtr := splitCommand(command);
        interpretCommand(cmdPtr, currentTree);
        if currentTree.all.child.all.next.all.name = To_Unbounded_String("test.txt") then
            Put_Line("TEST OK");
        else
            Put_Line("TEST NOT OK");
        end if;

        --Seventh test
        Put_line("## SEVENTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("rm -r sup");
        cmdPtr := splitCommand(command);
        interpretCommand(cmdPtr, currentTree);
        if currentTree.all.child = null then
            Put_Line("TEST OK");
        else
            Put_Line("TEST NOT OK");
        end if;

        --Eigth test
        Put_line("## EIGTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("rm /oi/test.txt");
        cmdPtr := splitCommand(command);
        interpretCommand(cmdPtr, currentTree);
        if anotherAgainNode.all.child = null then
            Put_Line("TEST OK");
        else
            Put_Line("TEST NOT OK");
        end if;

        --Ninth test
        Put_line("## NINTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("vim /oi/test.txt 30");
        cmdPtr := splitCommand(command);
        interpretCommand(cmdPtr, currentTree);
        if anotherAgainNode.all.child.all.size = 30 then
            Put_Line("TEST OK");
        else
            Put_Line("TEST NOT OK");
        end if;

        --Tenth test
        Put_line("## TENTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("help");
        cmdPtr := splitCommand(command);
        Put_line("##### BEGIN TEST_HELP #####");
        Put_line("TEST OK : cd          Change directory  ");
        Put_Line("          help        Display commands");
        Put_Line("          ls          Display folder(s) and file(s)");
        Put_Line("          mkdir       Create a folder");
        Put_Line("          pwd         Display the current directory");
        Put_Line("          rm          Remove folder or file");
        Put_Line("          touch       Create a file");
        Put_Line("          vim         Change size of the file");
        interpretCommand(cmdPtr, currentTree);

        --Eleventh test
        Put_Line("## ELEVENTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("rm -r sup sup");
        cmdPtr := splitCommand(command);
        Put_Line("TEST OK : too many arguments");
        interpretCommand(cmdPtr, currentTree);

        --Twelth test
        Put_Line("## TWELTH TEST ##");
        initTreeForTest;
        command := To_Unbounded_String("test");
        cmdPtr := splitCommand(command);
        Put_Line("TEST OK : unrecognized command : test");
        interpretCommand(cmdPtr, currentTree);

        Put_line("##### END TEST_INTERPRETCOMMAND #####");
    end test_interpretCommand;
    -- ########## TESTS FOR PACKAGE P_DMS ##########
end p_test;