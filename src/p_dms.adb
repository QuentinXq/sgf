with p_tree, p_commandfuncs,Ada.Strings.unbounded, Ada.Text_IO.Unbounded_IO;
use p_tree, p_commandfuncs,Ada.Strings.unbounded, Ada.Text_IO.Unbounded_IO;

package body p_dms is

    procedure pwd(currentTree : in NodePtr) is
        path : unbounded_string;
    begin
        -- On regarde si le Noeud courant n'est pas null
        if currentTree /= null then
            -- On récupère le chemin absolu du Noeud courant
            path := GetAbsoluteDir(currentTree);
            -- On affiche le chemin absolu
            put_line(path);
        -- Sinon on affiche que le Noeud courant est null
        else
            Put_Line(To_Unbounded_String("Current Directory null"));
        end if;
    end pwd;


    function cd(currentTree : in out NodePtr; path : in out StringPtr) return NodePtr is
    begin
        -- On regarde si le Noeud courant n'est pas null
        if currentTree /= null then
            -- On retourne le résultat du changement du Noeud courant lié au chemin
            return changeCurrentTree(currentTree, path);
        -- Sinon on retourne le Noeud courant
        else
            return currentTree;
        end if;
    end cd;

    procedure ls(currentTree : in out NodePtr; path : in out StringPtr; option : in unbounded_string) is
        currentTreeCopy : NodePtr;
    begin
        -- On affecte le Noeud courant à une variable temporaire afin de ne pas modifié la varible globale currentTree
        currentTreeCopy := currentTree;
        -- On regarde si la liste chainée du chemin n'est pas null
        if path /= null then
            -- On change de Noeud courant
            currentTreeCopy := cd(currentTreeCopy, path);
            -- On boucle tant que l'élément suivant de la liste n'est pas null
            while path.all.next /= null loop
                path := path.all.next;
            end loop;
            -- On regarde si le nom du Noeud et le nom du chemin sont les mêmes
            if path.all.name = currentTreeCopy.all.name then
                -- On list les éléments du Noeud courant
                listChildLevel(currentTreeCopy, option);
            end if;
        -- Sinon on list les éléments sur le Noeud courant rentré en paramètre
        else
            listChildLevel(currentTreeCopy, option);
        end if;
    end ls;

    procedure mkdir(currentTree : in out NodePtr; path : in out StringPtr) is
        currentTreeCopy : NodePtr;
        newNodeName : unbounded_string;
        dirFile : String(1..4) := "drwx";
    begin
        -- On affecte le Noeud courant à une variable temporaire afin de ne pas modifié la varible globale currentTree
        currentTreeCopy := currentTree;
        -- On récupère le dernier élément de la liste chainée du chemin
        newNodeName := getLastString(path);
        -- On regarde si la liste chainée de chemin n'est pas null
        if path /= null then
            -- On regarde si le prochain élément n'est pas null
            if path.all.next /= null then
                -- On supprime le dernier élément de la liste
                path := popLastString(path);
            -- Sinon on affecte la valeur '.' qui signifie le même Noeud
            else
                path.all.name := To_Unbounded_String(".");
            end if;
            -- On change de Noeud courant
            currentTreeCopy := changeCurrentTree(currentTreeCopy, path);
            -- On ajoute au niveau enfant du Noeud courant un nouveau Noeud
            addTree(currentTreeCopy, newNodeName, dirFile);
        -- Sinon on indique qu'il manque un ou des arguments
        else
            Put_Line(To_Unbounded_String("missing argument(s)"));
        end if;
    end mkdir;

    procedure touch(currentTree : in out NodePtr; path : in out StringPtr) is
        currentTreeCopy : NodePtr;
        newNodeName : unbounded_string;
        dirFile : String(1..4) := "-rwx";
    begin
        -- On affecte le Noeud courant à une variable temporaire afin de ne pas modifié la varible globale currentTree
        currentTreeCopy := currentTree;
        -- On récupère le dernier élément de la liste chainée du chemin
        newNodeName := getLastString(path);
        -- On regarde si la liste chainée de chemin n'est pas null
        if path /= null then
            -- On regarde si le prochain élément n'est pas null
            if path.all.next /= null then
                -- On supprime le dernier élément de la liste
                path := popLastString(path);
            -- Sinon on affecte la valeur '.' qui signifie le même Noeud
            else
                path.all.name := To_Unbounded_String(".");
            end if;
            -- On change de Noeud courant
            currentTreeCopy := changeCurrentTree(currentTreeCopy, path);
            -- On ajoute au niveau enfant du Noeud courant un nouveau Noeud
            addTree(currentTreeCopy, newNodeName, dirFile);
        -- Sinon on indique qu'il manque un ou des arguments
        else
            Put_Line(To_Unbounded_String("missing argument(s)"));
        end if;
    end touch;

    procedure rm(currentTree : in out NodePtr; path : in out StringPtr; option : in unbounded_string) is
        currentTreeCopy : NodePtr;
        removeNodeName : unbounded_string;
        dirFile : unbounded_string;
    begin
        -- On affecte le Noeud courant à une variable temporaire afin de ne pas modifié la varible globale currentTree
        currentTreeCopy := currentTree;
        -- On récupère le dernier élément de la liste chainée du chemin
        removeNodeName:= getLastString(path);
        -- On regarde si la liste chainée de chemin n'est pas null
        if path /= null then
            -- On regarde si le prochain élément n'est pas null
            if path.all.next /= null then
                -- On supprime le dernier élément de la liste
                path := popLastString(path);
            -- Sinon on affecte la valeur '.' qui signifie le même Noeud
            else
                path.all.name := To_Unbounded_String(".");
            end if;
            -- On change de Noeud courant
            currentTreeCopy := changeCurrentTree(currentTreeCopy, path);
            -- On regarde si l'option de récursivité est présente
            if option = To_Unbounded_String("-r") then
                -- On défini l'option dirFile sur (d)ossier
                dirFile := To_Unbounded_String("d");
            -- Sinon on défini l'option dirFile sur (-)fichier
            else
                dirFile := To_Unbounded_String("-");
            end if;
            -- On enlève le Noeud souhaité
            removeTree(currentTreeCopy, removeNodeName, dirFile);
        -- Sinon on indique qu'il manque un ou des arguments
        else
            Put_Line(To_Unbounded_String("missing argument(s)"));
        end if;
    end rm;

    procedure vim(currentTree : in out NodePtr; path : in out StringPtr; size : in Integer) is
        currentTreeCopy : NodePtr;
        removeNodeName : unbounded_string;
    begin
        -- On regarde si la liste chainée de chemin n'est pas null
        if path /= null then
            -- On affecte le Noeud courant à une variable temporaire afin de ne pas modifié la varible globale currentTree
            currentTreeCopy := currentTree;
            -- On récupère le dernier élément de la liste chainée du chemin
            removeNodeName:= getLastString(path);
            -- On regarde si le prochain élément n'est pas null
            if path.all.next /= null then
                -- On supprime le dernier élément de la liste
                path := popLastString(path);
            -- Sinon on affecte la valeur '.' qui signifie le même Noeud
            else
                path.all.name := To_Unbounded_String(".");
            end if;
            -- On change de Noeud courant
            currentTreeCopy := changeCurrentTree(currentTreeCopy, path);
            -- On regarde si le Noeud enfant n'est pas null
            if currentTreeCopy.all.child /= null then
                -- On va au Noeud enfant du Noeud courant
                currentTreeCopy := currentTreeCopy.all.child;
                -- On boucle tant que le Noeud courant n'est pas null
                while currentTreeCopy /= null loop
                    -- On regarde si le nom du Noeud courant est le même que le dernier élément de la liste de chemin
                    if currentTreeCopy.all.name = removeNodeName then
                        -- On change de taille le Noeud
                        changeNodeSize(currentTreeCopy, size);
                    end if;
                    currentTreeCopy := currentTreeCopy.all.next;
                end loop;
            end if;
        -- Sinon on indique qu'il manque un ou des arguments
        else
            Put_Line(To_Unbounded_String("missing argument(s)"));
        end if;
    end vim;


    procedure help is
    begin
        -- On affiche l'aide
        Put_Line(To_Unbounded_String("cd") & To_Unbounded_String("      Change directory"));
        Put_Line(To_Unbounded_String("help") & To_Unbounded_String("    Display commands"));
        Put_Line(To_Unbounded_String("ls") & To_Unbounded_String("      Display folder(s) and file(s)"));
        Put_Line(To_Unbounded_String("mkdir") & To_Unbounded_String("   Create a folder"));
        Put_Line(To_Unbounded_String("pwd") & To_Unbounded_String("     Display the current directory"));
        Put_Line(To_Unbounded_String("rm") & To_Unbounded_String("      Remove folder or file"));
        Put_Line(To_Unbounded_String("touch") & To_Unbounded_String("   Create a file"));
        Put_Line(To_Unbounded_String("vim") & To_Unbounded_String("     Change size of the file"));
    end help;


    procedure interpretCommand(cmdPointer : in out CommandPtr; currentTree : in out NodePtr) is
        firstElement : unbounded_string;
        path : StringPtr;
        option : unbounded_string;
        size : Integer := 0;
        count : Integer := 0;
        cmdPointerCopy : CommandPtr;
    begin
        cmdPointerCopy := cmdPointer;
        -- On boucle tant que la liste chainée de commande n'est pas null
        while cmdPointerCopy /= null loop
            -- On va à l'élément suivant et on compte le nombre d'occurrences de la liste
            cmdPointerCopy := cmdPointerCopy.all.next;
            count := count + 1;
        end loop;
        -- On regarde si le nombre d'occurrences est inférieur ou égal à 3
        if count <= 3 then
            -- On affecte le premier élément de la liste de commande
            firstElement := cmdPointer.all.commandPart;
            -- On regarde si l'élément suivant de la liste n'est pas null
            if cmdPointer.all.next /= null then
                -- On regarde si le nom de la commande de l'élément suivant est '-l' ou '-r'
                if cmdPointer.all.next.all.commandPart = "-l" or cmdPointer.all.next.all.commandPart = "-r" then
                    -- On défini le nom de l'élément suivant comme étant une option
                    option := cmdPointer.all.next.all.commandPart;
                    -- On regarde si l'élément suivant suivant de la liste de commande n'est pas null
                    if cmdPointer.all.next.all.next /= null then
                        -- On récupère la liste de chemin de à partir du nom de l'élément suivant suivant de la liste de commande
                        path := splitPath(cmdPointer.all.next.all.next.all.commandPart);
                    end if;
                -- Sinon on regarde si l'élément suivant suivant de la liste de commande n'est pas null
                elsif cmdPointer.all.next.all.next /= null then
                    -- On affecte le nom de l'élément suivant suivant de la liste de commande à la variable taille
                    size := Integer'Value (To_String(cmdPointer.all.next.all.next.all.commandPart));
                    -- On récupère la liste de chemin de à partir du nom de l'élément suivant de la liste de commande
                    path := splitPath(cmdPointer.all.next.all.commandPart);
                -- Sinon on récupère la liste de chemin de à partir du nom de l'élément suivant de la liste de commande et défini l'option à ""
                else
                    path := splitPath(cmdPointer.all.next.all.commandPart);
                    option := To_Unbounded_String("");
                end if;
            -- Sinon on affecte null à la liste de chemin
            else
                path := null;
            end if;
            -- On énumère ici tous les cas possibles de commandes avec des if et elsif
            if firstElement = To_Unbounded_String("pwd") then
                pwd(currentTree);
            elsif firstElement = To_Unbounded_String("mkdir") then
                mkdir(currentTree, path);
            elsif firstElement = To_Unbounded_String("cd") then
                currentTree := cd(currentTree, path);
            elsif firstElement = To_Unbounded_String("ls") then
                ls(currentTree, path, option);
            elsif firstElement = To_Unbounded_String("touch") then
                touch(currentTree, path);
            elsif firstElement = To_Unbounded_String("rm") then
                rm(currentTree, path, option);
            elsif firstElement = To_Unbounded_String("help") then
                help;
            elsif firstElement = To_Unbounded_String("vim") then
                vim(currentTree, path, size);
            -- Si aucune des commandes n'a été reconnues alors on affiche que la commande entré n'a pas été reconnu
            else
                if firstElement /= To_Unbounded_String("") then
                    put_line(To_Unbounded_String("unrecognized command : ") & firstElement);
                end if;
            end if;
        -- Sinon on indique qu'il y a eu trop d'arguments dans la commande
        else
            Put_Line(To_Unbounded_String("too many arguments"));
        end if;
    end interpretCommand;

end p_dms;