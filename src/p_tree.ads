with Ada.Strings.unbounded;
use Ada.Strings.unbounded;

package p_tree is
    -- (******************************** TAD ********************************)
    -- (* Arbre : Pointeur NodePtr sur Enregistrement Node                  *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Types :                                                           *)
    -- (*       NodePtr : Pointeur sur Node                                 *)
    -- (*       Node : Enregistrement                                       *)
    -- (*           name : unbounded_string, nom du Noeud                   *)
    -- (*           extension : unbounded_string, extension du Noeud        *)
    -- (*           rights : String(1..4), droits du Noeud                  *)
    -- (*           parentFolder : NodePtr, pointeur sur le Noeud parent    *)
    -- (*           child : NodePtr, pointeur sur le Noeud enfant           *)
    -- (*           next : NodePtr, pointeur sur le Noeud suivant de même   *)
    -- (*                           niveau                                  *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Constructeurs :                                                   *)
    -- (*       initTree : Initialise l'arbre à null                        *)
    -- (*       isEmptyTree : Regarde si l'arbre est vide                   *)
    -- (*       changeCurrentTree : Change le pointeur courant sur un Noeud *)
    -- (*                           donné                                   *)
    -- (*       addTree : Ajoute un Noeud dans le niveau enfant du Noeud    *)
    -- (*                 courant                                           *)
    -- (*       removeTree : Supprime un Noeud dans le niveau enfant du     *)
    -- (*                    courant                                        *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Accesseurs :                                                      *)
    -- (*       getAbsoluteDir : Permet d'obtenir le chemin absolue à       *)
    -- (*                        partir d'un Noeud                          *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Modifieurs :                                                      *)
    -- (*       changeNodeName : Change le nom du noeud donné               *)
    -- (*       changeNodeExtension : Change l'extension du noeud donné     *)
    -- (*       changeNodeRights : Change les droits du noeud donné         *)
    -- (*       changeNodeSize : Change la taille du noeud donné            *)
    -- (*********************************************************************)
    type Node;
    type NodePtr is access Node;
    type Node is record
        name : unbounded_string;
        extension : unbounded_string;
        rights : String(1..4);
        size : Integer;
        parentFolder : NodePtr;
        child : NodePtr;
        next : NodePtr;
    end record;

    -- (******************************** TAD ********************************)
    -- (* Liste chainée : Pointeur StringPtr sur R_Path                     *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Types :                                                           *)
    -- (*       StringPtr : Pointeur sur R_Path                             *)
    -- (*       R_Path : Enregistrement                                     *)
    -- (*           name : unbounded_string, nom de l'élément               *)
    -- (*           isAbsolute : Boolean, element est à l'origine d'un      *)
    -- (*                                 chemin absolu                     *)
    -- (*           next : StringPtr, Pointeur sur le prochain élément      *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Constructeurs :                                                   *)
    -- (*       insertPath : Ajoute un nouvel élément à la liste            *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Accesseurs :                                                      *)
    -- (*       getLastString : Permet d'obtenir le dernier élément de la   *)
    -- (*                       liste chainée                               *)
    -- (* ----------------------------------------------------------------- *)
    -- (* Modifieurs :                                                      *)
    -- (*       popLastString : Permet de retourner une liste chainée       *)
    -- (*                       privée de son dernier élément               *)
    -- (*********************************************************************)
    type R_Path;
    type StringPtr is access R_Path;
    type R_Path is record
            name : unbounded_string;
            isAbsolute : Boolean;
            next : StringPtr;
    end record;


    function createRootDMS(currentTree : in out NodePtr) return NodePtr;
    -- (*********************************************************************)
    -- (* fonction createRootDMS                                            *)
    -- (* sémantique :  création de l'arbre avec des noeuds déjà à          *)
    -- (*               l'intérieur                                         *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, le premier noeud de l'arbre          *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (* retour : NodePtr, noeud de l'arbre                                *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_createRootDMS                            *)               

    procedure initTree(currentTree : out NodePtr);
    -- (*********************************************************************)
    -- (* procedure initTree                                                *)
    -- (* sémantique :  initialiser l'arbre en rendant définissant le       *)
    -- (*               pointeur sur l'arbre null                           *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, le premier noeud de l'arbre          *)
    -- (* pré-conditions :  currentTree défini en tant que variable globale *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_initTree                                 *) 

    function isEmptyTree(currentTree : in NodePtr) return Boolean;
    -- (*********************************************************************)
    -- (* fonction isEmptyTree                                              *)
    -- (* sémantique :  permet de regarder si l'arbre est vide/null         *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (* retour : Boolean, arbre est null ou non                           *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_isEmptyTree                              *) 

    function changeCurrentTree(currentTree : in out NodePtr; path : in out StringPtr) return NodePtr;
    -- (*********************************************************************)
    -- (* fonction changeCurrentTree                                        *)
    -- (* sémantique :  permet de retourner le noeud indiqué par le chemin  *)
    -- (*               (path) fourni en paramètre                          *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (*       path : StringPtr, la liste chaînée du chemin                *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (* post-conditions : currentTree se trouve dans l'arbre              *)
    -- (* exceptions : chemin introuvable                                   *)
    -- (* retour : NodePtr, noeud de l'arbre                                *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_changeCurrentTree                        *) 

    procedure addTree(currentTree : in out NodePtr; name : in unbounded_string; dirFile : in String);
    -- (*********************************************************************)
    -- (* procedure addTree                                                 *)
    -- (* sémantique :  permet d'ajouter un nouveau noeud au niveau enfant  *)
    -- (*               du currentTree                                      *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (*       name : unbounded_strig, nom du nouveau noeud                *)
    -- (*       dirFile : String, permet de définir si le nouveau noeud est *)
    -- (*                         un dossier ou un fichier                  *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (*                   dirFile = '-' ou 'd'                            *)
    -- (*                   name non vide                                   *)
    -- (* post-conditions : currentTree a un nouvelle enfant                *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_addTree                                  *)

    procedure removeTree(currentTree : in out NodePtr; name : in unbounded_string; dirFile : in unbounded_string);
    -- (*********************************************************************)
    -- (* procedure removeTree                                              *)
    -- (* sémantique :  permet de supprimer noeud au niveau enfant du       *)
    -- (*               currentTree en fonction de son nom                  *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (*       name : unbounded_strig, nom du noeud à supprimer            *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (*                   name dans les enfants de currenTree             *)
    -- (* post-conditions : currentTree a un enfant en moins                *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_removeTree                               *)

    procedure changeNodeName(currentTree: in out NodePtr; name : in unbounded_string);
    -- (*********************************************************************)
    -- (* procedure changeNodeName                                          *)
    -- (* sémantique :  permet de changer le nom du fichier ou dossier      *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (*       name : unbounded_string, nom                                *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (* post-conditions :                                                 *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_changeNodeName                           *)

    procedure changeNodeExtension(currentTree: in out NodePtr; extension : in unbounded_string);
    -- (*********************************************************************)
    -- (* procedure changeNodeExtension                                     *)
    -- (* sémantique :  permet de changer l'extension du fichier ou dossier *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (*       extension : unbounded_string, extension                     *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (* post-conditions :                                                 *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_changeNodeExtension                      *)

    procedure changeNodeRights(currentTree: in out NodePtr; rights : in String);
    -- (*********************************************************************)
    -- (* procedure changeNodeRights                                        *)
    -- (* sémantique :  permet de changer les droits du fichier ou dossier  *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (*       rights : String, droits                                     *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (* post-conditions :                                                 *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_changeNodeRights                         *)

    procedure changeNodeSize(currentTree: in out NodePtr; size : in Integer);
    -- (*********************************************************************)
    -- (* procedure changeNodeName                                          *)
    -- (* sémantique :  permet de changer la taille du fichier ou dossier   *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (*       size : Integer, taille                                      *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (* post-conditions :                                                 *)
    -- (* exceptions : /                                                    *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_changeNodeSize                           *)

    function getAbsoluteDir(currentTree : in NodePtr) return unbounded_string;
    -- (*********************************************************************)
    -- (* fonction getAbsoluteDir                                           *)
    -- (* sémantique :  permet de retourner le chemin absolue depuis le     *)
    -- (*               chemin courant                                      *)
    -- (* paramètre :                                                       *)
    -- (*       currentTree : NodePtr, un noeud de l'arbre                  *)
    -- (* pré-conditions :  currentTree précédemment initialisé             *)
    -- (* post-conditions :                                                 *)
    -- (* exceptions : /                                                    *)
    -- (* retour : unbounded_string, chaine de charactère du chemin absolu  *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_getAbsoluteDir                           *)

    function getLastPathPtr(path : in out StringPtr) return StringPtr;
    -- (*********************************************************************)
    -- (* fonction getLastPath                                              *)
    -- (* sémantique :  permet de récupérer le dernier élément de la liste  *)
    -- (*               chaînée de unbounded_string                         *)
    -- (* paramètre :                                                       *)
    -- (*       path : StringPtr, chemin sous forme d'une liste chainée     *)
    -- (* pré-conditions :  path non null                                   *)
    -- (* post-conditions :                                                 *)
    -- (* exceptions : /                                                    *)
    -- (* retour : StringPtr, dernier élément de la chaîne                  *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_getLastPath                              *)

    function getExtensionOfFile(name : in unbounded_string) return unbounded_string;
    -- (*********************************************************************)
    -- (* fonction getExtensionOfFile                                       *)
    -- (* sémantique :  permet de récupérer l'extension d'un fichier        *)
    -- (* paramètre :                                                       *)
    -- (*       name : unbounded_string, nom du fichier                     *)
    -- (* pré-conditions :  name non vide                                   *)
    -- (* post-conditions : /                                               *)
    -- (* exceptions : /                                                    *)
    -- (* retour : unbouded_string, l'extension du fichier                  *)
    -- (*********************************************************************)
    -- (* tests :   procedure test_getExtensionOfFile                       *)

end p_tree;