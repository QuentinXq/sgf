with Ada.Text_IO;
use Ada.Text_IO;

with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;

with Ada.Float_Text_IO;
use Ada.Float_Text_IO;

with Ada.Strings.unbounded;
use Ada.Strings.unbounded;

with Ada.Text_IO.Unbounded_IO;
use Ada.Strings.unbounded;

with p_commandfuncs;
use p_commandfuncs;

with p_tree;
use p_tree;

with P_DMS;
use P_DMS;

procedure main_sgf is
    procedure commandMenu is
        choice : unbounded_string;
        absolutePath : unbounded_string;
        command : unbounded_string;
        currentTree : NodePtr;
        path : unbounded_string;
        pathPtr : StringPtr;
        option : unbounded_string;
        name : unbounded_string;
        size : Integer;
    begin
        currentTree := createRootDMS(currentTree);
        loop
            Put_Line("############################");
            Put_Line("#       Command Menu       #");
            Put_Line("############################");
            Put_Line("What do you want to do ? (Please enter the number of the command)");
            Put_Line("1. cd : change directory");
            Put_Line("2. pwd : print working directory");
            Put_Line("3. ls : list files and directories");
            Put_Line("4. touch : create file");
            Put_Line("5. mkdir : create directory");
            Put_Line("6. rm : remove folder or file");
            Put_Line("7. vim : change size of the file");
            Put("Choice : ");
            choice := To_Unbounded_String(Get_Line);
            if (To_String(choice) = "1") then
                absolutePath := getAbsoluteDir(currentTree);
                Put_Line("Current Directory : " & To_String(absolutePath));
                Put("Change directory : ");
                path := To_Unbounded_String(Get_Line);
                pathPtr := splitPath(path);
                currentTree := cd(currentTree, pathPtr);
                absolutePath := getAbsoluteDir(currentTree);
                Put_Line("Current Directory : " & To_String(absolutePath));
            elsif (To_String(choice) = "2") then
                Put("Current Directory : ");
                pwd(currentTree);
            elsif (To_String(choice) = "3") then
                absolutePath := getAbsoluteDir(currentTree);
                Put_Line("Current Directory : " & To_String(absolutePath));
                Put("Option : ");
                option := To_Unbounded_String(Get_Line);
                absolutePath := getAbsoluteDir(currentTree);
                path := currentTree.all.name;
                pathPtr := splitPath(path);
                ls(currentTree, pathPtr, option);
            elsif (To_String(choice) = "4") then
                absolutePath := getAbsoluteDir(currentTree);
                Put_Line("Current Directory : " & To_String(absolutePath));
                Put("Change directory and add folder name : ");
                path := To_Unbounded_String(Get_Line);
                pathPtr := splitPath(path);
                touch(currentTree, pathPtr);
            elsif (To_String(choice) = "5") then
                absolutePath := getAbsoluteDir(currentTree);
                Put_Line("Current Directory : " & To_String(absolutePath));
                Put("Change directory and add file name : ");
                path := To_Unbounded_String(Get_Line);
                pathPtr := splitPath(path);
                mkdir(currentTree, pathPtr);
            elsif (To_String(choice) = "6") then
                absolutePath := getAbsoluteDir(currentTree);
                Put_Line("Current Directory : " & To_String(absolutePath));
                Put("Option : ");
                option := To_Unbounded_String(Get_Line);
                Put("Change directory and add folder or file to remove : ");
                path := To_Unbounded_String(Get_Line);
                pathPtr := splitPath(path);
                rm(currentTree, pathPtr, option);
            elsif (To_String(choice) = "7") then
                absolutePath := getAbsoluteDir(currentTree);
                Put_Line("Current Directory : " & To_String(absolutePath));
                Put("Change directory and file to change : ");
                path := To_Unbounded_String(Get_Line);
                pathPtr := splitPath(path);
                Put("New size of file : ");
                size := Integer'Value(Get_Line);
                vim(currentTree, pathPtr, size);
            end if;
            new_line;
            exit when To_String(choice) = "exit";
        end loop;
    end commandMenu;

    procedure commandPrompt is
        command : unbounded_string;
        cmdPtr : CommandPtr;
        currentTree : NodePtr;
        absolutePath : unbounded_string;
        inputBefore : unbounded_string;
    begin
        currentTree := createRootDMS(currentTree);
        Put_Line("############################");
        Put_Line("#      Command Prompt      #");
        Put_Line("############################");
        loop
            begin
                absolutePath := getAbsoluteDir(currentTree);
                inputBefore := absolutePath & To_Unbounded_String("$ ");
                Put("QRostalski@ComputerN7:" & To_String(inputBefore));
                command := To_Unbounded_String(Get_Line);
                cmdPtr := splitCommand(command);
                interpretCommand(cmdPtr, currentTree);
                exception
                    when others => Put_Line("random problem :)");
            end;
            exit when To_String(command) = "exit";
        end loop;
    end commandPrompt;

    procedure mainMenu is
        choice : unbounded_string;
        length : Integer := 1;
    begin
        Put_Line("############################");
        Put_Line("#      Welcome to SGF      #");
        Put_Line("############################");
        Put_Line("You can choose between :");
        Put_Line("(P)rompt command");
        Put_Line("(M)enu");
        Put("Choice : ");
        choice := To_Unbounded_String(Get_Line);
        if (To_String(choice) = "M") then  
            commandMenu;
        elsif (To_String(choice) = "P") then
            commandPrompt;
        elsif (To_String(choice) = "exit") then
            null;
        end if;
    end mainMenu;

begin

    mainMenu;

end main_sgf;