package p_test is

    procedure initTreeForTest;

    -- ########## TESTS FOR PACKAGE P_TREE ##########
    procedure test_createRootDMS;
    procedure test_initTree;
    procedure test_isEmptyTree;
    procedure test_changeCurrentTree;
    procedure test_addTree;
    procedure test_removeTree;
    procedure test_changeNodeName;
    procedure test_changeNodeRights;
    procedure test_changeNodeExtension;
    procedure test_changeNodeSize;
    procedure test_getAbsoluteDir;
    procedure test_getLastPathPtr;
    procedure test_getExtensionOfFile;
    -- ########## TESTS FOR PACKAGE P_TREE ##########

    -- ########## TESTS FOR PACKAGE P_COMMANDFUNCS ##########
    procedure test_splitCommand;
    procedure test_splitPath;
    procedure test_insertCommand;
    procedure test_insertPath;
    procedure test_listChildLevel;
    procedure test_popLastString;
    procedure test_getLastString;
    -- ########## TESTS FOR PACKAGE P_COMMANDFUNCS ##########

    -- ########## TESTS FOR PACKAGE P_DMS ##########
    procedure test_pwd;
    procedure test_cd;
    procedure test_ls;
    procedure test_mkdir;
    procedure test_touch;
    procedure test_rm;
    procedure test_vim;
    procedure test_help;
    procedure test_interpretCommand;
    -- ########## TESTS FOR PACKAGE P_DMS ##########
end p_test;