with Ada.Text_IO,Ada.Integer_Text_IO,Ada.Float_Text_IO,Ada.Strings.unbounded,Ada.Text_IO.Unbounded_IO,p_tree,Ada.Strings.unbounded;
use Ada.Text_IO,Ada.Integer_Text_IO,Ada.Float_Text_IO,Ada.Strings.unbounded,Ada.Text_IO.Unbounded_IO,p_tree,Ada.Strings.unbounded;

package body p_commandfuncs is

      function splitCommand(command : in unbounded_string) return CommandPtr is
        tempCmd : unbounded_string;
        cmdPtr : CommandPtr := NULL;
        low, high : Integer;
    begin
        -- On définit des marqueurs qui vont être utilisé afin de split la commande
        low := 1;
        high := 1;
        -- On boucle pour tous les caractères dans la commande
        for i in 1..Length(command) loop
            -- On regarde si le caractère est le dernier caractère et si la liste chainée est null
            if i = Length(command) and cmdPtr /= null then
                -- On affecte l'ancienne valeur de high + 2 à low
                low := high + 2;
                -- On affecte à high le dernier caractère
                high := i;
                -- On récupère la chaine de caractères entre les deux marqueurs
                Unbounded_Slice(command, tempCmd, low, high);
                -- On l'ajoute à la liste chainée
                insertCommand(tempCmd, cmdPtr);
            end if;
            -- On regarde si le caractère est un espace
            if Element(command, i) = ' ' then
                -- On affecte high au caractère précédent l'espace
                high := i-1;
                -- On récupère la chaine de caractères entre les deux marqueurs
                Unbounded_Slice(command, tempCmd, low, high);
                -- On l'ajoute à la liste chainée
                insertCommand(tempCmd, cmdPtr);
                low := i+1;
            end if;
        end loop;
        -- Si la liste est vide après le traitement
        if cmdPtr = null then
            -- On ajoute la commande entière dans la liste
            insertCommand(command, cmdPtr);
        end if;
        return cmdPtr;
    end splitCommand;


    function splitPath(path : in out unbounded_string) return StringPtr is
        tempCmd : unbounded_string;
        pathPtr : StringPtr := NULL;
        low, high : Integer;
        isAbsolute : Boolean := True;
    begin
        -- On définit des marqueurs qui vont être utilisé afin de split le chemin
        low:= 1;
        high := -1;
        -- On regarde si le chemin n'est pas égal à vide
        if path /= To_Unbounded_String("") then
            -- On regarde si le premier élément du chemin est un '/'
            if Element(path, 1) = '/' then
                -- On supprime le '/'
                Delete(path, 1, 1);
                tempCmd := To_Unbounded_String("/");
                -- On ajoute le '/' dans la liste chainée
                pathPtr := new R_Path'(tempCmd, isAbsolute, null);
                isAbsolute := False;
            else
                isAbsolute := False;
            end if;
            -- On boucle pour tous les caractères dans la commande
            for i in 1..Length(path) loop
                -- On regarde si le caractère est le dernier caractère
                if i = Length(path) then
                    -- On affecte l'ancienne valeur de high + 2 à low
                    low := high + 2;
                    -- On affecte à high le dernier caractère
                    high := i;
                    -- On récupère la chaine de caractères entre les deux marqueurs
                    Unbounded_Slice(path, tempCmd, low, high);
                    -- On l'ajoute à la liste chainée
                    insertPath(tempCmd, pathPtr, isAbsolute);
                end if;
                -- On regarde si le caractère est un '/'
                if Element(path, i) = '/' then
                    -- On affecte high au caractère précédent l'espace
                    high := i-1;
                    -- On récupère la chaine de caractères entre les deux marqueurs
                    Unbounded_Slice(path, tempCmd, low, high);
                    -- On l'ajoute à la liste chainée
                    insertPath(tempCmd, pathPtr, isAbsolute);
                    low := i+1;
                end if;
            end loop;
        -- Sinon on ajoute '.' à la liste chainée
        else
            pathPtr := new R_Path'(To_Unbounded_String("."), isAbsolute, null);
        end if;
        -- On regarde si la liste chainée est null après le traitement
        if pathPtr = null then
            -- On ajoute le chemin entière dans la liste
            pathPtr := new R_Path'(To_Unbounded_String("."), isAbsolute, null);
        end if;
        return pathPtr;
    end splitPath;


    procedure insertCommand(command : in unbounded_string; cmdPtr : in out CommandPtr) is
    tmpPtr : CommandPtr;
    begin
        -- On regarde si la liste chainée est null
        if cmdPtr = null then
            -- On défini le premier élément de la chaine
            cmdPtr := new R_Command'(command, null);
        else
            -- On défini une variable temporaire afin de ne pas perdre la tête de la liste
            tmpPtr := cmdPtr;
            -- On boucle tant que l'élément suivant de la liste n'est pas null
            while tmpPtr.all.next /= null loop
                tmpPtr := tmpPtr.all.next;
            end loop;
            -- On ajoute à l'élément suivant de la liste un nouvel élément
            tmpPtr.all.next := new R_Command'(command, null);
        end if;
    end insertCommand;


    procedure insertPath(path : in out unbounded_string; pathPtr : in out StringPtr; isAbsolute : in Boolean) is
    tmpPtr : StringPtr;
    begin
        -- On regarde si la liste chainée est null
        if pathPtr = null then
            -- On défini le premier élément de la chaine
            pathPtr := new R_Path'(path, isAbsolute, null);
        else
            -- On défini une variable temporaire afin de ne pas perdre la tête de la liste
            tmpPtr := pathPtr;
            -- On boucle tant que l'élément suivant de la liste n'est pas null
            while tmpPtr.all.next /= null loop
                tmpPtr := tmpPtr.all.next;
            end loop;
            -- On ajoute à l'élément suivant de la liste un nouvel élément
            tmpPtr.all.next := new R_Path'(path, isAbsolute, null);
        end if;
    end insertPath;


    procedure listChildLevel(currentTree : in NodePtr; option : in unbounded_string) is
        currentTreeCopy : NodePtr;
        name : unbounded_string;
        ext : unbounded_string;
        rights : String(1..4);
        size : Integer;
        leftSide : unbounded_string;
        rightSide : unbounded_string;
    begin
        -- On va au Noeud enfant et on le place dans une variable temporaire
        currentTreeCopy := currentTree.all.child;
        -- On boucle tant que l'élément n'est pas null
        while currentTreeCopy /= null loop
            -- On défini les différents éléments composants un Noeud
            name := currentTreeCopy.all.name;
            ext := currentTreeCopy.all.extension;
            rights := currentTreeCopy.all.rights;
            size := currentTreeCopy.all.size;
            -- On regarde si l'option de listing détaillé est présente
            if option = To_Unbounded_String("-l") then
                -- On affiche les différents éléments
                Put(To_Unbounded_String(rights) & To_Unbounded_String(" "));
                Put(size);
                Put(" ");
                Put(ext & To_Unbounded_String(" "));
                Put(name);
                new_line;
            else
            -- Sinon on affiche juste le nom
                Put(To_String(name & " "));
            end if;
            -- On va à l'élément suivant de même niveau
            currentTreeCopy := currentTreeCopy.all.next;
        end loop;
        -- On regarde si l'option de listing n'est pas présente
        if option /= To_Unbounded_String("-l") then
            -- On ajoute une ligne
            new_line;
        end if;
    end listChildLevel;


    function popLastString(path : in out StringPtr) return StringPtr is
        tmp : StringPtr;
    begin
        -- On regarde si la liste est null
        if path /= null then
            -- On défini une variable temporaire pour ne pas perdre la tête
            tmp := path;
            -- On regarde si le prochain élément de la liste est null
            if tmp.all.next = null then
                -- On défini la liste à null vu que le dernier élément était le seul de la liste
                path := null;
                return path;
            else
                -- On boucle tant que l'élément suivant suivant n'est pas null
                while tmp.all.next.all.next /= null loop
                    tmp := tmp.all.next;
                end loop;
                -- On rend le dernier élément null
                tmp.all.next := null;
                return path;
            end if;
        else
            return path;
        end if;
    end popLastString;

    function getLastString(path : in StringPtr) return unbounded_string is
        tmpPath : StringPtr;
    begin
        -- On défini une variable temporaire pour ne pas perdre la tête
        tmpPath := path;
        -- On regarde si la liste chainée est null
        if tmpPath /= null then
            -- On boucle tant que l'élément suivant n'est pas null
            while tmpPath.all.next /= null loop
                tmpPath := tmpPath.all.next;
            end loop;
            -- On retourne le nom du dernier élément
            return tmpPath.all.name;
        -- Sinon on retourne ""
        else
            return To_Unbounded_String("");
        end if;
    end getLastString;


end p_commandfuncs;